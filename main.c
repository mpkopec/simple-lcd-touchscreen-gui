/**
 * \mainpage
 *
 * \section intro Introduction
 * For a tutorial and other explanations please refer to this document [here will be the link].
 *
 * \cond
 * \link<guiDefinitions.h> GUI API \endlink
 *
 * \link<lcd.h> LCD API \endlink
 *
 * \link<lcdExtension.h> Extension for LCD API \endlink
 *
 * \link<touchpanel.h> Touchpanel API \endlink
 *
 * \link<touchpanelExtension.h> Event handling \endlink
 *
 * \link<main.c> Application \endlink
 * \endcond
 */

/**
 * \file main.c
 * \brief The main file of the demo application, contains all handlers and everything that is
 * needed for the demo app to work.
 *
 * This file contains the demo application for using GUI, LCD and touchscreen on the LandTiger board.
 * It is a simple MS Paint alike program and illustrates all options available in included libraries.
 */

#include "gui/guiDefinitions.h"
#include "lcd/lcd.h"
#include "lcd/lcdExtension.h"
#include "touchpanel/touchpanel.h"
#include "touchpanel/touchpanelExtension.h"

/**
 * \enum FigureName
 * This enum contains information about the figure name. It is responsible for containing the information
 * about what will be drawn on the next event. Depending on this enum the program will either draw and store
 * points for drawing rectangles/circles and then draw the whole figure or it will put 2x2 squares
 * continuously if set to FREE_DRAW. Names are self-explanatory.
 */
typedef enum FigureName {
	CIRCLE,
	FILLED_CIRCLE,
	RECTANGLE,
	FILLED_RECTANGLE,
	FREE_DRAW
} FigureName;


/// Global variable containing, what to draw on the next tap event.
FigureName figureToDraw = FREE_DRAW;
/**
 * This variable contains the first point for drawing rectangles or circles. If the circle is being drawn
 * the first point is the center and the distance between the first point and the second will be interpreted
 * as the radius. If the rectangle is being drawn the first point is considered left top corner and the second
 *  - bottom right.
 */
Coordinate p = {.x = 0xffff, .y = 0xffff};
/**
 * Variable contains current color of the drawn figures.
 */
uint16_t currentColor = LCDBlack;
/**
 * \var cActiveColorButton
 * This variable contains currently active button for radio-type select. It is used to reset colors
 * of active buttons and make them look inactive.
 */
/**
 * \var cActiveFigureButton
 * This variable contains currently active button for radio-type select. It is used to reset colors
 * of active buttons and make them look inactive.
 */
GUIButtonWidget *cActiveColorButton, *cActiveFigureButton;


/**
 * Function makes the previous button inactive and redraws it, then it changes state of currently tapped
 * button to active and makes it current button. Redrawing of the current button that also changed state
 * will be done by the GUI API as it checks if the button which is tapped changed state. Function is
 * necessary as radio groups of buttons were not implemented in the API.
 *
 * \param cActive Double pointer to currently active button.
 * \param cButton Pointer to tapped button that will be currently active from now on.
 */
void resetRadioTypeButtons ( GUIButtonWidget **cActive, GUIButtonWidget *cButton ) {
	if ( *cActive != cButton ) {
		(*cActive)->state = IDLE;
		guiRedrawButtonThatMayCollide( (*cActive)->parentPanel, (*cActive)->buttonIndex );
	}
	cButton->state = ACTIVE;
	(*cActive) = cButton;
}

/**
 * Short auxiliary function that resets global variable containing the last tapped point.
 */
void resetContainedPoint () {
	p.x = 0xffff;
	p.y = 0xffff;
}

/**
 * A necessary function for calculating square root that is counted as an integer. Pixels are discrete
 * and the microcontroller doesn't have floating point arithmetics implemented. Code was taken from:
 * <a href="http://www.lbebooks.com/downloads/exportal/Verilog_NEXYS_Example24.pdf"
 * title="Source of the integer square root function">
 * http://www.lbebooks.com/downloads/exportal/Verilog_NEXYS_Example24.pdf
 * </a>.
 *
 * \param a Number being a base for the square root.
 */
uint16_t isqrt( uint16_t a ){
	uint32_t square = 1;
	uint32_t delta = 3;
	while(square <= a){
		square = square + delta;
		delta = delta + 2;
	}
	return (delta/2 - 1);
}

/**
 * Default handler for leaving the touchscreen - releasing finger or stylus. Firstly it sets clipping region
 * of the LCD to the free area where the canvas is. Then it handles rectangle and circle drawing setting
 * necessary global variables and drawing what is requested. This handles only drawing figures, not free drawing!
 * If the previous point exists then we shall draw a circle or a rectangle, if not - we set the previous point
 * and wait for another tap.
 *
 * For more info about handlers and default handlers please refer to the GUI API documentation.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 */
void defLeaveHandler ( uint16_t x, uint16_t y ) {
	ClippingRegion clip = { .x0 = 0, .y0 = 0, .width = 200, .height = 200 };
	lcdSetClippingRegion( clip );

	if ( figureToDraw != FREE_DRAW ) {
		if ( p.x != 0xffff && p.y != 0xffff ) {
			uint16_t r = isqrt( (p.x - x)*(p.x - x) + (p.y - y)*(p.y - y) );
			switch ( figureToDraw ) {
				case CIRCLE:
				lcdDrawCircle( p.x, p.y, r, currentColor );
				break;

				case FILLED_CIRCLE:
				lcdDrawFilledCircle( p.x, p.y, r, currentColor, currentColor );
				break;

				case RECTANGLE:
				lcdDrawRectangle( p.x, p.y, x, y, currentColor );
				break;

				case FILLED_RECTANGLE:
				lcdDrawFilledRectangle( p.x, p.y, x, y, currentColor, currentColor );
			}

			p.x = 0xffff;
			p.y = 0xffff;
		} else {
			p.x = x;
			p.y = y;

			lcdSetPixel( x, y, currentColor );
		}
	}

	lcdSetDefaultClippingRegion();
}

/**
 * Default handler for continuously holding and dragging stylus on the touchscreen. It de facto draws
 * points being 2x2 squares until #figureToDraw is set to FREE_DRAW.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 */
void defPressHandler ( uint16_t x, uint16_t y ) {
	if ( figureToDraw == FREE_DRAW ) {
		ClippingRegion clip = { .x0 = 0, .y0 = 0, .width = 200, .height = 200 };
		lcdSetClippingRegion( clip );

		lcdSetPixel( x, y, currentColor );
		lcdSetPixel( x+1, y, currentColor );
		lcdSetPixel( x, y+1, currentColor );
		lcdSetPixel( x+1, y+1, currentColor );

		lcdSetDefaultClippingRegion();
	}
}

/**
 * Handler for the clearing button, it fills the canvas area with the currently chosen color.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void clrHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	ClippingRegion clip = { .x0 = 0, .y0 = 0, .width = 200, .height = 200 };
	lcdSetClippingRegion( clip );

	lcdFillRectangularArea( 0, 0, 199, 199, currentColor );
	resetContainedPoint();
	lcdSetDefaultClippingRegion();
}

/**
 * Handler that sets #figureToDraw to circle.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void circleHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();

	figureToDraw = CIRCLE;

	resetRadioTypeButtons( &cActiveFigureButton, cButton );
}

/**
 * Handler that sets #figureToDraw to filled circle.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void filledCircleHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();

	figureToDraw = FILLED_CIRCLE;

	resetRadioTypeButtons( &cActiveFigureButton, cButton );
}

/**
 * Handler that sets #figureToDraw to rectangle.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void rectHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();

	figureToDraw = RECTANGLE;

	resetRadioTypeButtons( &cActiveFigureButton, cButton );
}

/**
 * Handler that sets #figureToDraw to filled rectangle.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void filledRectHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();

	figureToDraw = FILLED_RECTANGLE;

	resetRadioTypeButtons( &cActiveFigureButton, cButton );
}

/**
 * Handler that sets #figureToDraw to free drawing.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void freestyleHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();

	figureToDraw = FREE_DRAW;

	resetRadioTypeButtons( &cActiveFigureButton, cButton );
}

/**
 * Handler that sets #currentColor to the color of the button that was tapped.
 *
 * \param x X coordinate of the event.
 * \param y Y coordinate of the event.
 * \param cButton Pointer to the button on which event appeared.
 */
void changeColorHandler ( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) {
	resetContainedPoint();
	currentColor = cButton->base.idleColors.bgColor;

	resetRadioTypeButtons( &cActiveColorButton, cButton );
}

/**
 * Function creating GUI controls. It adds panels to the list and then widgets to panels. It assigns colors
 * and coordinates. It also sets default #currentColor and #figureToDraw values and makes buttons co-related
 * with these values active.
 */
void createGUI ( void ) {
	uint8_t cid;
	uint8_t cb;

	GUIRectangle coords;
	GUIColorDecoration idleColors, activeColors;


	coords.x0 = 0;
	coords.y0 = 0;
	coords.width = 120;
	coords.height = 240;
	idleColors.bgColor = LCDPastelBlue;
	idleColors.borderColor = LCDPastelBlue;
	idleColors.textColor = LCDBlue;
	activeColors.bgColor = LCDGrey;
	activeColors.borderColor = LCDBlack;
	activeColors.textColor = LCDWhite;
	cid = guiAddPanel( coords, idleColors, ALIGN_HORIZONTAL_RIGHT, ALIGN_VERTICAL_TOP, PANEL_VERTICAL, BUTTONALIGN_VERTICAL_TOP, BUTTONALIGN_HORIZONTAL_CENTER, 5, 3, 1 );

	// General widget properties
	coords.width = 108;
	coords.height = 0;
	idleColors.bgColor = 0xffff;
	idleColors.borderColor = LCDBlack;
	idleColors.textColor = 0xffff;
	guiAddText( cid, coords, idleColors, TEXTALIGN_CENTER, 5, "Drawing menu", 1 );

	// Difference between buttons and text
	idleColors.textColor = LCDBlack;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 5, "circle", 1, 0, circleHandler );
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 5, "fill. circle", 1, 0, filledCircleHandler );
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 5, "rectangle", 1, 0, rectHandler );
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 5, "fill. rect.", 1, 0, filledRectHandler );
	cb = guiAddButton( cid, coords, idleColors, activeColors, ACTIVE, 5, "freestyle", 1, 0, freestyleHandler );
	cActiveFigureButton = guiGetButton( cid, cb ); // Active button by default


	// Second panel properties
	idleColors.bgColor = LCDPastelBlue;
	idleColors.borderColor = LCDPastelBlue;
	coords.width = 320;
	coords.height = 40;
	cid = guiAddPanel( coords, idleColors, ALIGN_HORIZONTAL_LEFT, ALIGN_VERTICAL_BOTTOM, PANEL_HORIZONTAL, BUTTONALIGN_VERTICAL_CENTER, BUTTONALIGN_HORIZONTAL_CENTER, 5, 3, 1 );

	// Second panel widget properties
	coords.width = 0;
	coords.height = 0;
	idleColors.bgColor = 0xffff;
	idleColors.textColor = 0xffff;
	idleColors.textColor = LCDBlack;

	// First button
	coords.width = 40;
	coords.height = 28;
	idleColors.borderColor = LCDBlack;
	activeColors.bgColor = LCDGrey;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 5, "Clr", 1, clrHandler, 0 );

	// Caption text for choosing color
	coords.width = 60;
	coords.height = 0;
	idleColors.textColor = LCDWhite;
	guiAddText( cid, coords, idleColors, TEXTALIGN_LEFT, 5, "Color:", 1 );

	// General properties for choosing color buttons
	coords.width = 19;
	coords.height = 19;
	idleColors.textColor = LCDWhite;
	idleColors.bgColor = LCDBlack;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	cb = guiAddButton( cid, coords, idleColors, activeColors, ACTIVE, 0, " ", 1, changeColorHandler, 0 );
	cActiveColorButton = guiGetButton( cid, cb ); // Active color by default

	// Rest of the buttons, assigning colors
	idleColors.bgColor = LCDGrey;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDWhite;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDGreen;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDYellow;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDRed;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDMagenta;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDBlue;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
	idleColors.bgColor = LCDBlueSea;
	idleColors.borderColor = idleColors.bgColor;
	activeColors = idleColors;
	activeColors.borderColor = ~idleColors.bgColor;
	guiAddButton( cid, coords, idleColors, activeColors, IDLE, 0, " ", 1, changeColorHandler, 0 );
}

/**
 * Function in which user can put his code and it will be executed every iteration of the real-time loop.
 * \warning User should never put here an infinite loop, because it will suspend event handling, since it is
 * checked every iteration at the beginning.
 */
void doOtherStuff ( void ) {

}

/**
 * Main function, user shouldn't change that or do it carefully making sure that his actions won't cause
 * any damage to the event handling in the real-time loop.
 */
int main(void)
{
	uint8_t i = 255;


	// LCD initialization
	lcdInitializtion();


	// -------------------------------- INTRO --------------------------------
	// Magenta -> blue
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( i, 0, 255 );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Blue -> cyan
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( 0, 255-i, 255 );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Cyan -> green
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( 0, 255, i );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Green -> yellow
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( 255-i, 255, 0 );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Yellow -> red
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( 255, i, 0 );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Red -> black
 	for ( i = 255; i > 0; i -= 15 ) {
 		uint16_t  color = rgb565( i, 0, 0 );

 		lcdClear( color );
 		lcdDrawText( 3, 92, "Aplikacja demonstracyjna dla kolorowego", ~color );
 		lcdDrawText( 19, 112, "wyswietlacza dotykowego pracujacego", ~color );
 		lcdDrawText( 75, 132, "w systemie wbudowanym", ~color );
 		lcdDrawText( 210, 220, "Maciej Kopec", ~color );
 	}

 	// Touch panel - initialization and calibration
 	touchpanelInit();
	touchpanelCalibrate();

	lcdClear( LCDWhite );


	// ----------------------------- END OF INTRO -----------------------------

	// Create GUI controls
	createGUI();
	// Draw GUI from the bottom (whole)
	guiRedraw( 0 );

//	//Debug stuff
//	GUIPanel *panel = guiGetPanel( 0 );
//	char str[30];
//	sprintf( str, "x0=%d, y0=%d", panel->coords.x0, panel->coords.y0 );
//	lcdDrawText( 0, 0, str, LCDBlack, LCDWhite );


	while (1) {
		checkTap( defPressHandler, defLeaveHandler );
		doOtherStuff();
	}

	return 0;
}
