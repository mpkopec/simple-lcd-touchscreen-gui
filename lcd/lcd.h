/**
 * \file lcd.h
 * \brief File contains all functions needed to read and write data from and to LCD.
 * \author AVRman (http://www.powermcu.com)
 * \author Maciej Kope� (corrections, refactoring, new functions)
 * \version 1.1b
 *
 * It creates complete API for reading and writing from and to LCD, as well as it provides elementary
 * commands such as lcdDrawLine() and lcdSetPoint().
 */


#ifndef __GLCD_H 
#define __GLCD_H


#include "LPC17xx.h"

/**
 * \defgroup LCDPins LCD pins and connections
 * Defines specific pins used to communicate with the LCD module, should not be changed until the board or schematic
 * is changed.
 * @{
 */
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_EN		(1 << 19)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_LE		(1 << 20)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_DIR		(1 << 21)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_CS      (1 << 22)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_RS		(1 << 23)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_WR		(1 << 24)
/** Group documentation here: \ref LCDPins See datasheet for pin descriptions */
#define PIN_RD		(1 << 25)   
/** @} */

/**
 * \defgroup LCDPinSetMacros LCD pin setting macros
 * Defines macros used to set specific values to corresponding pins.
 * @{
 */
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_EN(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_EN) : (LPC_GPIO0->FIOCLR = PIN_EN));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_LE(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_LE) : (LPC_GPIO0->FIOCLR = PIN_LE));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_DIR(x)  ((x) ? (LPC_GPIO0->FIOSET = PIN_DIR) : (LPC_GPIO0->FIOCLR = PIN_DIR));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_CS(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_CS) : (LPC_GPIO0->FIOCLR = PIN_CS));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_RS(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_RS) : (LPC_GPIO0->FIOCLR = PIN_RS));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_WR(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_WR) : (LPC_GPIO0->FIOCLR = PIN_WR));
/** Group documentation here: \ref LCDPinSetMacros See datasheet for pin descriptions */
#define LCD_RD(x)   ((x) ? (LPC_GPIO0->FIOSET = PIN_RD) : (LPC_GPIO0->FIOCLR = PIN_RD));
/** @} */

/**
 * Defines initial rotation of the screen.
 */
#define DISP_ORIENTATION  90  /* angle 0 90 */

#if  ( DISP_ORIENTATION == 90 ) || ( DISP_ORIENTATION == 270 )

/**
 * Screen width
 */
#define  LCD_MAX_X  320
/**
 * Screen height
 */
#define  LCD_MAX_Y  240   

#elif  ( DISP_ORIENTATION == 0 ) || ( DISP_ORIENTATION == 180 )

/**
 * Screen width
 */
#define  LCD_MAX_X  240
/**
 * Screen height
 */
#define  LCD_MAX_Y  320   

#endif


/**
 * \defgroup LCDColors Predefined colors for the LCD
 *
 * Colors in rgb565 format defined as 16-bit unsigned numbers ready to use by the programmer
 * @{
 */
/** See \ref LCDColors */
#define LCDWhite         	0xFFFF
/** See \ref LCDColors */
#define LCDBlack         	0x0000
/** See \ref LCDColors */
#define LCDGrey				0xa534
/** See \ref LCDColors */
#define LCDBlue				0x001F
/** See \ref LCDColors */
#define LCDBlueSea	        0x05BF
/** See \ref LCDColors */
#define LCDPastelBlue       0x051F
/** See \ref LCDColors */
#define LCDViolet       	0xB81F
/** See \ref LCDColors */
#define LCDMagenta	       	0xF81F
/** See \ref LCDColors */
#define LCDRed          	0xF800
/** See \ref LCDColors */
#define LCDGinger		 	0xFAE0
/** See \ref LCDColors */
#define LCDGreen        	0x07E0
/** See \ref LCDColors */
#define LCDCyan         	0x7FFF
/** See \ref LCDColors */
#define LCDYellow       	0xFFE0
/**
 * @}
 */

/**
 * Similar to rgb565(), except it  takes 8-bit unsigned integers and simply shifts out 3 LSB's.
 */
#define RGB565CONVERT(red, green, blue)\
(uint16_t)( (( red   >> 3 ) << 11 ) | \
(( green >> 2 ) << 5  ) | \
( blue  >> 3 ))

/**
 * \struct ClippingRegion
 * Structure created for containing clipping region rectangle.
 */
typedef struct ClippingRegion {
	uint16_t x0;
	uint16_t y0;
	uint16_t width;
	uint16_t height;
} ClippingRegion;

/**
 * The function initializes the LCD, needs to be executed before any drawing operation is done.
 *
 * \note Screen may not initialize properly after the first switching voltage on. The LCD stays
 * white then. If this occurs just switch the voltage off and on again, problem should be solved.
 */
void lcdInitializtion(void);

/**
 * The function fills the screen with given color.
 *
 * \param Color New background color.
 */
void lcdClear(uint16_t Color);

/**
 * The function sets clipping region which will affect all drawing functions except lcdClear(). Everything
 * that would be drawn outside this region will not be displayed.
 *
 * \note After any use of this function user needs to execute lcdSetDefaultClippingRegion() for recovering
 * default clipping region. It is necessary for the GUI to draw properly.
 *
 * \param clip Clipping area.
 */
void lcdSetClippingRegion( ClippingRegion clip );

/**
 * The function resets clipping region to the whole screen, meaning that the programmer can draw wherever he wants to
 * on the LCD.
 */
void lcdSetDefaultClippingRegion();

/**
 * The function gets currently set clipping region e.g. in order to check currently set dimensions of the "canvas".
 *
 * \return Returns structure being currently set as the clipping region.
 */
ClippingRegion lcdGetClippingRegion();

/**
 * The function returns current set color from the given coordinates.
 *
 * \return 16-bit rgb565 color.
 *
 * \param Xpos X coordinate of the point.
 * \param Ypos Y coordinate of the point.
 */
uint16_t lcdGetPixel(uint16_t Xpos,uint16_t Ypos);

/**
 * The function draws a point of given color with given coordinates.
 *
 * \param Xpos X coordinate of the point.
 * \param Ypos Y coordinate of the point.
 * \param color 16-bit rgb565 color.
 */
void lcdSetPixel(uint16_t Xpos,uint16_t Ypos,uint16_t color);

/**
 * The function draws a line of specified color connecting to given points.
 *
 * \param x0 X coordinate of the start point.
 * \param y0 Y coordinate of the start point.
 * \param x1 X coordinate of the end point.
 * \param y1 Y coordinate of the end point.
 * \param color 16-bit rgb565 color of the line.
 */
void lcdDrawLine( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1 , uint16_t color );

/**
 * The function draws a char on the screen with given coordinates.
 *
 * \param Xpos X coordinate of the char - left top corner.
 * \param Ypos Y coordinate of the char - left top corner.
 * \param ASCII ASCII code of the char.
 * \param charColor 16-bit rgb565 color of the char itself.
 */
void lcdDrawChar( uint16_t Xpos, uint16_t Ypos, uint8_t ASCII, uint16_t charColor );

/**
 * The function draws whole text on the screen with given coordinates.
 *
 * \param Xpos X coordinate of the text - left top corner.
 * \param Ypos Y coordinate of the text - left top corner.
 * \param str C-string of the text.
 * \param Color 16-bit rgb565 color of the text itself.
 */
void lcdDrawText(uint16_t Xpos, uint16_t Ypos, uint8_t *str, uint16_t Color);

/**
 * The function fills a rectangular area specified by the programmer.
 *
 * \param x0 X of the rectangle's left top corner.
 * \param y0 Y of the rectangle's left top corner.
 * \param x1 X of the rectangle's right bottom corner.
 * \param y1 Y of the rectangle's right bottom corner.
 * \param color An integer representing color of the area's fill.
 */
void lcdFillRectangularArea ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color );

#endif
