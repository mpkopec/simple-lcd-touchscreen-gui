#include "lcd.h"
#include "lcdExtension.h"

uint16_t rgb565 ( uint8_t r, uint8_t g, uint8_t b ) {
	uint16_t r1, g1, b1;
	r1 = ((uint16_t)r * 31)/255;
	g1 = ((uint16_t)g * 63)/255;
	b1 = ((uint16_t)b * 31)/255;
	r1 = r1<<11;
	g1 = g1<<5;
	
	uint16_t rgb = r1 | g1 | b1;

	return rgb;
}

void lcdDrawFilledRectangle( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t borderColor, uint16_t fillColor ) {
	lcdFillRectangularArea( x0, y0, x1, y1, fillColor );
	lcdDrawLine( x0, y0, x1, y0, borderColor );
	lcdDrawLine( x0, y0, x0, y1, borderColor );
	lcdDrawLine( x0, y1, x1, y1, borderColor );
	lcdDrawLine( x1, y0, x1, y1, borderColor );
}

void lcdDrawRectangle( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t borderColor ) {
	lcdDrawLine( x0, y0, x1, y0, borderColor );
	lcdDrawLine( x0, y0, x0, y1, borderColor );
	lcdDrawLine( x0, y1, x1, y1, borderColor );
	lcdDrawLine( x1, y0, x1, y1, borderColor );
}

void lcdDrawCircle( uint16_t xm, uint16_t ym, uint16_t r, uint16_t borderColor) {
	int32_t x = -r, y = 0, err = 2-2*r;
	int32_t rp = r;
	do {
		lcdSetPixel((uint16_t)(xm-x), (uint16_t)(ym+y), borderColor); /*   I. Quadrant */
		lcdSetPixel((uint16_t)(xm-y), (uint16_t)(ym-x), borderColor); /*  II. Quadrant */
		lcdSetPixel((uint16_t)(xm+x), (uint16_t)(ym-y), borderColor); /* III. Quadrant */
		lcdSetPixel((uint16_t)(xm+y), (uint16_t)(ym+x), borderColor); /*  IV. Quadrant */
		rp = err;
		if (rp <= y) err += ++y*2+1;           /* e_xy+e_y < 0 */
		if (rp > x || err > y) err += ++x*2+1; /* e_xy+e_x > 0 or no 2nd y-step */
	} while (x < 0);
}

void lcdDrawFilledCircle( uint16_t xm, uint16_t ym, uint16_t r, uint16_t borderColor, uint16_t fillColor) {
	int32_t x = -r, y = 0, err = 2-2*r;
	int32_t rp = r;
	int32_t ystep, xstep;
	do {
		lcdSetPixel((uint16_t)(xm-x), (uint16_t)(ym+y), borderColor); /*   I. Quadrant */
		for ( xstep = xm+1; xstep < xm-x; xstep++ ) {
			lcdSetPixel( xstep, ym+y, fillColor );
		}
		
		lcdSetPixel((uint16_t)(xm-y), (uint16_t)(ym-x), borderColor); /*  II. Quadrant */
		for ( ystep = ym+1; ystep < ym-x; ystep++ ) {
			lcdSetPixel( xm-y, ystep, fillColor );
		}
		
		lcdSetPixel((uint16_t)(xm+x), (uint16_t)(ym-y), borderColor); /* III. Quadrant */
		for ( xstep = xm-1; xstep > xm+x; xstep-- ) {
			if ( xstep < 0 ) {
				break;
			}
			lcdSetPixel( xstep, ym-y, fillColor );
		}

		lcdSetPixel((uint16_t)(xm+y), (uint16_t)(ym+x), borderColor); /*  IV. Quadrant */
		for ( ystep = ym-1; ystep > ym+x; ystep-- ) {
			if ( ystep < 0 ) {
				break;
			}
			lcdSetPixel( xm+y, ystep, fillColor );
		}
		rp = err;
		if (rp <= y) err += ++y*2+1;           /* e_xy+e_y < 0 */
		if (rp > x || err > y) err += ++x*2+1; /* e_xy+e_x > 0 or no 2nd y-step */
	} while (x < 0);
	lcdSetPixel( xm, ym, fillColor );
}

void lcdDrawTriangle ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t borderColor ) {
	lcdDrawLine( x0, y0, x2, y2, borderColor );
	lcdDrawLine( x2, y2, x1, y1, borderColor );
	lcdDrawLine( x0, y0, x1, y1, borderColor );
}

void lcdDrawCross ( uint16_t x, uint16_t y, uint16_t color ) {
	lcdDrawLine( x-2, y-2, x+2, y+2, color );
	lcdDrawLine( x-2, y+2, x+2, y-2, color );
}
