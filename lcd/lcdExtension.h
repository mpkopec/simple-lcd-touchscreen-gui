/**
 * \file lcdExtension.h
 * \brief File contains functions extending standard lcdDrawLine(), lcdSetPoint() and lcdFillRectangularArea().
 *
 * This functions may be used for more advanced drawing operations executed on the LCD.
 */

#include "lcd.h"

#pragma once

/**
 * Functions converting unsigned 8-bit integers representing R, G, and B parts of a color, assuming that their values
 * are between 0 and 255, to one 16-bit number representing RGB565 color.
 *
 * \return 16-bit color ready to send to the LCD.
 *
 * \param r A number between 0 and 31 representing R part of the color.
 * \param g A number between 0 and 63 representing G part of the color.
 * \param b A number between 0 and 31 representing B part of the color.
 */
uint16_t rgb565 ( uint8_t r, uint8_t g, uint8_t b );

/**
 * Functions draws rectangle from 2 given points.
 *
 * \param x0 X of the rectangle's left top corner.
 * \param y0 Y of the rectangle's left top corner.
 * \param x1 X of the rectangle's right bottom corner.
 * \param y1 Y of the rectangle's right bottom corner.
 * \param borderColor An integer representing color of the rectangle's border.
 */
void lcdDrawRectangle ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t borderColor );

/**
 * Function draws a rectangle filled with programmer-specified color.
 *
 * \param x0 X of the rectangle's left top corner.
 * \param y0 Y of the rectangle's left top corner.
 * \param x1 X of the rectangle's right bottom corner.
 * \param y1 Y of the rectangle's right bottom corner.
 * \param borderColor An integer representing color of the rectangle's border.
 * \param fillColor An integer representing color of the rectangle's filling.
 */
void lcdDrawFilledRectangle ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t borderColor, uint16_t fillColor );

/**
 * Function draws a circle on the LCD screen.
 *
 * \author Code was taken from http://members.chello.at/~easyfilter/bresenham.html
 * \author Maciej Kope� - small corrections and adjustments needed to be done before drawing on this specific LCD.
 *
 * \param xm X of the circle's center.
 * \param ym Y of the circle's center.
 * \param r Radius of the circle given in pixels.
 * \param borderColor Color of the circle's border.
 */
void lcdDrawCircle ( uint16_t xm, uint16_t ym, uint16_t r, uint16_t borderColor);

/**
 * Function draws a filled circle on the LCD screen.
 *
 * \author Code was taken from http://members.chello.at/~easyfilter/bresenham.html
 * \author Maciej Kope� - small corrections and filling algorithm implementation.
 *
 * \param xm X of the circle's center.
 * \param ym Y of the circle's center.
 * \param r Radius of the circle given in pixels.
 * \param borderColor Color of the circle's border.
 * \param fillColor Color of the circle's filling.
 */
void lcdDrawFilledCircle ( uint16_t xm, uint16_t ym, uint16_t r, uint16_t borderColor, uint16_t fillColor);

/**
 * Function draws a triangle with 3 given points. Border color is programmer-specified.
 *
 * \param x0 X of the triangle's first point.
 * \param y0 Y of the triangle's first point.
 * \param x1 X of the triangle's second point.
 * \param y1 Y of the triangle's second point.
 * \param x2 X of the triangle's third point.
 * \param y2 Y of the triangle's third point.
 * \param borderColor An integer representing color of the triangle's border.
 */
void lcdDrawTriangle ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2, uint16_t borderColor );

/**
 * Function draws a simple, 5-pixel wide cross with given color.
 *
 * \param x X coordinate of the crosses center.
 * \param y Y coordinate of the crosses center.
 * \param color An integer representing color of the cross.
 */
void lcdDrawCross ( uint16_t x, uint16_t y, uint16_t color );
