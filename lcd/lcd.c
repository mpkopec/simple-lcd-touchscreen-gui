/****************************************Copyright (c)**************************************************                         
**
**                                 http://www.powermcu.com
**
**--------------File Info-------------------------------------------------------------------------------
** File name:			GLCD.c
** Descriptions:		Has been tested SSD1289��ILI9320��R61505U��SSD1298��ST7781��SPFD5408B��ILI9325��ILI9328��
**						HX8346A��HX8347A
**------------------------------------------------------------------------------------------------------
** Created by:			AVRman
** Created date:		2012-3-10
** Version:				1.3
** Descriptions:		The original version
**
**------------------------------------------------------------------------------------------------------
** Modified by:			
** Modified date:	
** Version:
** Descriptions:		
********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "lcd.h"
#include "asciiLib.h"


/* Private variables ---------------------------------------------------------*/
static uint8_t LCD_Code;
static const ClippingRegion defaultClip = { .x0 = 0, .y0 = 0, .width = LCD_MAX_X, .height = LCD_MAX_Y };
static ClippingRegion clip = { .x0 = 0, .y0 = 0, .width = LCD_MAX_X, .height = LCD_MAX_Y };

/* Private define ------------------------------------------------------------*/
#define  ILI9320    0  /* 0x9320 */
#define  ILI9325    1  /* 0x9325 */
#define  ILI9328    2  /* 0x9328 */
#define  ILI9331    3  /* 0x9331 */
#define  SSD1298    4  /* 0x8999 */
#define  SSD1289    5  /* 0x8989 */
#define  ST7781     6  /* 0x7783 */
#define  LGDP4531   7  /* 0x4531 */
#define  SPFD5408B  8  /* 0x5408 */
#define  R61505U    9  /* 0x1505 0x0505 */
#define  HX8346A	10 /* 0x0046 */  
#define  HX8347D    11 /* 0x0047 */
#define  HX8347A    12 /* 0x0047 */	
#define  LGDP4535   13 /* 0x4535 */  
#define  SSD2119    14 /* 3.5 LCD 0x9919 */

/*******************************************************************************
* Function Name  : Lcd_Configuration
* Description    : Configures LCD Control lines
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdConfiguration(void)
{
	/* Configure the LCD Control pins */
	
	/* EN = P0.19 , LE = P0.20 , DIR = P0.21 , CS = P0.22 , RS = P0.23 , RS = P0.23 */
	/* RS = P0.23 , WR = P0.24 , RD = P0.25 , DB[0.7] = P2.0...P2.7 , DB[8.15]= P2.0...P2.7 */  

	LPC_GPIO0->FIODIR   |= 0x03f80000;
	LPC_GPIO0->FIOSET    = 0x03f80000;
}


void lcdSetClippingRegion( ClippingRegion clipping ) {
	clip = clipping;
}


void lcdSetDefaultClippingRegion() {
	clip = defaultClip;
}


ClippingRegion lcdGetClippingRegion() {
	return clip;
}

/*******************************************************************************
* Function Name  : LCD_Send
* Description    : LCDд���
* Input          : - byte: byte to be sent
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdSend (uint16_t byte)
{
	LPC_GPIO2->FIODIR |= 0xFF;          /* P2.0...P2.7 Output */
	LCD_DIR(1)		   				    /* Interface A->B */
	LCD_EN(0)	                        /* Enable 2A->2B */
	LPC_GPIO2->FIOPIN =  byte;          /* Write D0..D7 */
	LCD_LE(1)                         
	LCD_LE(0)							/* latch D0..D7	*/
	LPC_GPIO2->FIOPIN =  byte >> 8;     /* Write D8..D15 */
}

/*******************************************************************************
* Function Name  : wait_delay
* Description    : Delay Time
* Input          : - nCount: Delay Time
* Output         : None
* Return         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void wait_delay(int count)
{
	while(count--);
}

/*******************************************************************************
* Function Name  : LCD_Delay
* Description    : Delay Time
* Input          : - nCount: Delay Time
* Output         : None
* Return         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void delay_ms(uint16_t ms)
{
	uint16_t i,j;
	for( i = 0; i < ms; i++ )
	{
		for( j = 0; j < 1141; j++ );
	}
}

/*******************************************************************************
* Function Name  : LCD_Read
* Description    : LCD�����
* Input          : - byte: byte to be read
* Output         : None
* Return         : ���ض�ȡ�������
* Attention		 : None
*******************************************************************************/
uint16_t lcdRead (void)
{
	uint16_t value;
	
	LPC_GPIO2->FIODIR &= ~(0xFF);              /* P2.0...P2.7 Input */
	LCD_DIR(0);		   				           /* Interface B->A */
	LCD_EN(0);	                               /* Enable 2B->2A */
	wait_delay(1);							   /* delay some times */
	value = LPC_GPIO2->FIOPIN0;                /* Read D8..D15 */
	LCD_EN(1);	                               /* Enable 1B->1A */
	wait_delay(10);							   /* delay some times */
	value = (value << 8) | LPC_GPIO2->FIOPIN0; /* Read D0..D7 */
	LCD_DIR(1);
	return  value;
}

/*******************************************************************************
* Function Name  : LCD_WriteIndex
* Description    : LCDд�Ĵ�����ַ
* Input          : - index: �Ĵ�����ַ
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdWriteIndex(uint16_t index)
{
	LCD_CS(0);
	LCD_RS(0);
	LCD_RD(1);
	lcdSend( index ); 
	wait_delay(1);
	LCD_WR(0);  
	wait_delay(1);
	LCD_WR(1);
	LCD_CS(1);
}

/*******************************************************************************
* Function Name  : LCD_WriteData
* Description    : LCDд�Ĵ������
* Input          : - index: �Ĵ������
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdWriteData(uint16_t data)
{				
	LCD_CS(0);
	LCD_RS(1);   
	lcdSend( data );
	LCD_WR(0);     
	wait_delay(1);
	LCD_WR(1);
	LCD_CS(1);
}

/*******************************************************************************
* Function Name  : LCD_ReadData
* Description    : ��ȡ���������
* Input          : None
* Output         : None
* Return         : ���ض�ȡ�������
* Attention		 : None
*******************************************************************************/
uint16_t lcdReadData(void)
{ 
	uint16_t value;
	
	LCD_CS(0);
	LCD_RS(1);
	LCD_WR(1);
	LCD_RD(0);
	value = lcdRead();
	
	LCD_RD(1);
	LCD_CS(1);
	
	return value;
}

/*******************************************************************************
* Function Name  : LCD_WriteReg
* Description    : Writes to the selected LCD register.
* Input          : - LCD_Reg: address of the selected register.
*                  - LCD_RegValue: value to write to the selected register.
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdWriteReg(uint16_t LCD_Reg,uint16_t LCD_RegValue)
{ 
	/* Write 16-bit Index, then Write Reg */  
	lcdWriteIndex(LCD_Reg);         
	/* Write 16-bit Reg */
	lcdWriteData(LCD_RegValue);  
}

/*******************************************************************************
* Function Name  : LCD_WriteReg
* Description    : Reads the selected LCD Register.
* Input          : None
* Output         : None
* Return         : LCD Register Value.
* Attention		 : None
*******************************************************************************/
uint16_t lcdReadReg(uint16_t LCD_Reg)
{
	uint16_t LCD_RAM;
	
	/* Write 16-bit Index (then Read Reg) */
	lcdWriteIndex(LCD_Reg);
	/* Read 16-bit Reg */
	LCD_RAM = lcdReadData();      	
	return LCD_RAM;
}

/*******************************************************************************
* Function Name  : LCD_SetCursor
* Description    : Sets the cursor position.
* Input          : - Xpos: specifies the X position.
*                  - Ypos: specifies the Y position. 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdSetCursor(uint16_t Xpos, uint16_t Ypos)
{
    #if  ( DISP_ORIENTATION == 90 ) || ( DISP_ORIENTATION == 270 )
	
 	uint16_t temp = Xpos;

	Xpos = Ypos;
	Ypos = ( LCD_MAX_X - 1 ) - temp;

	#elif  ( DISP_ORIENTATION == 0 ) || ( DISP_ORIENTATION == 180 )
		
	#endif

	lcdWriteReg(0x004e, Xpos );
	lcdWriteReg(0x004f, Ypos );
}


/*******************************************************************************
* Function Name  : LCD_Initializtion
* Description    : Initialize TFT Controller.
* Input          : None
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdInitializtion(void)
{
	uint16_t DeviceCode;
	
	lcdConfiguration();
	delay_ms(100);
	DeviceCode = lcdReadReg(0x0000);		/* ��ȡ��ID	*/
	/* ��ͬ����IC ��ʼ����ͬ */
	if( DeviceCode == 0x8989 )
	{
	    LCD_Code = SSD1289;
	    lcdWriteReg(0x0000,0x0001);    delay_ms(50);   /* �򿪾��� */
	    lcdWriteReg(0x0003,0xA8A4);    delay_ms(50);
	    lcdWriteReg(0x000C,0x0000);    delay_ms(50);
	    lcdWriteReg(0x000D,0x080C);    delay_ms(50);
	    lcdWriteReg(0x000E,0x2B00);    delay_ms(50);
	    lcdWriteReg(0x001E,0x00B0);    delay_ms(50);
	    lcdWriteReg(0x0001,0x2B3F);    delay_ms(50);   /* ���������320*240 0x2B3F */
	    lcdWriteReg(0x0002,0x0600);    delay_ms(50);
	    lcdWriteReg(0x0010,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0011,0x6070);    delay_ms(50);   /* ������ݸ�ʽ 16λɫ ���� 0x6070 */
	    lcdWriteReg(0x0005,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0006,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0016,0xEF1C);    delay_ms(50);
	    lcdWriteReg(0x0017,0x0003);    delay_ms(50);
	    lcdWriteReg(0x0007,0x0133);    delay_ms(50);
	    lcdWriteReg(0x000B,0x0000);    delay_ms(50);
	    lcdWriteReg(0x000F,0x0000);    delay_ms(50);   /* ɨ�迪ʼ��ַ */
	    lcdWriteReg(0x0041,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0042,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0048,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0049,0x013F);    delay_ms(50);
	    lcdWriteReg(0x004A,0x0000);    delay_ms(50);
	    lcdWriteReg(0x004B,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0044,0xEF00);    delay_ms(50);
	    lcdWriteReg(0x0045,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0046,0x013F);    delay_ms(50);
	    lcdWriteReg(0x0030,0x0707);    delay_ms(50);
	    lcdWriteReg(0x0031,0x0204);    delay_ms(50);
	    lcdWriteReg(0x0032,0x0204);    delay_ms(50);
	    lcdWriteReg(0x0033,0x0502);    delay_ms(50);
	    lcdWriteReg(0x0034,0x0507);    delay_ms(50);
	    lcdWriteReg(0x0035,0x0204);    delay_ms(50);
	    lcdWriteReg(0x0036,0x0204);    delay_ms(50);
	    lcdWriteReg(0x0037,0x0502);    delay_ms(50);
	    lcdWriteReg(0x003A,0x0302);    delay_ms(50);
	    lcdWriteReg(0x003B,0x0302);    delay_ms(50);
	    lcdWriteReg(0x0023,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0024,0x0000);    delay_ms(50);
	    lcdWriteReg(0x0025,0x8000);    delay_ms(50);
	    lcdWriteReg(0x004f,0);        /* ����ַ0 */
	    lcdWriteReg(0x004e,0);        /* ����ַ0 */
	}
    delay_ms(50);   /* delay 50 ms */	
}

/*******************************************************************************
* Function Name  : LCD_Clear
* Description    : ����Ļ����ָ������ɫ��������������� 0xffff
* Input          : - Color: Screen Color
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdClear(uint16_t Color)
{
	uint32_t index;

	lcdSetCursor(0,0);

	lcdWriteIndex(0x0022);
	for( index = 0; index < LCD_MAX_X * LCD_MAX_Y; index++ )
	{
		lcdWriteData(Color);
	}
}

/******************************************************************************
* Function Name  : LCD_BGR2RGB
* Description    : RRRRRGGGGGGBBBBB ��Ϊ BBBBBGGGGGGRRRRR ��ʽ
* Input          : - color: BRG ��ɫֵ  
* Output         : None
* Return         : RGB ��ɫֵ
* Attention		 : �ڲ��������
*******************************************************************************/
uint16_t lcdBGR2RGB(uint16_t color)
{
	uint16_t  r, g, b, rgb;
	
	b = ( color>>0 )  & 0x1f;
	g = ( color>>5 )  & 0x3f;
	r = ( color>>11 ) & 0x1f;
	
	rgb =  (b<<11) + (g<<5) + (r<<0);
	
	return( rgb );
}

/******************************************************************************
* Function Name  : LCD_GetPoint
* Description    : ��ȡָ��������ɫֵ
* Input          : - Xpos: Row Coordinate
*                  - Xpos: Line Coordinate 
* Output         : None
* Return         : Screen Color
* Attention		 : None
*******************************************************************************/
uint16_t lcdGetPixel(uint16_t Xpos,uint16_t Ypos)
{
	uint16_t dummy;
	
	lcdSetCursor(Xpos,Ypos);
	lcdWriteIndex(0x0022);
	
	dummy = lcdReadData();   /* Empty read */
	dummy = lcdReadData();
	return  dummy;
}

/******************************************************************************
* Function Name  : LCD_SetPoint
* Description    : ��ָ����껭��
* Input          : - Xpos: Row Coordinate
*                  - Ypos: Line Coordinate 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdSetPixel(uint16_t Xpos,uint16_t Ypos,uint16_t color)
{
	if( Xpos >= clip.x0 + clip.width || Xpos < clip.x0 || Ypos >= clip.y0 + clip.height || Ypos < clip.y0 )
	{
		return;
	}
	lcdSetCursor(Xpos,Ypos);
	lcdWriteReg(0x0022,color);
}


void lcdFillRectangularArea ( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color ) {
	uint16_t tmp;
	if ( x0 > x1 ) {
		tmp = x0;
		x0 = x1;
		x1 = tmp;
	}
	if ( y0 > y1 ) {
		tmp = y0;
		y0 = y1;
		y1 = tmp;
	}
	
	if ( x0 >= clip.x0 + clip.width || y0 >= clip.y0 + clip.height || x1 < clip.x0 || y1 < clip.y0 ) {
		return;
	}
	
	if ( x0 < clip.x0 ) {
		x0 = clip.x0;
	}
	if ( x1 > clip.x0 + clip.width - 1 ) {
		x1 = clip.x0 + clip.width - 1;
	}
	if ( y0 < clip.y0 ) {
		y0 = clip.y0;
	}
	if ( y1 > clip.y0 + clip.height - 1 ) {
		y1 = clip.y0 + clip.height - 1;
	}
	
	uint16_t x, y;
	for ( x = x0; x <= x1; x++ ) {
		lcdSetCursor(x,y0); 
		lcdWriteIndex(0x0022);
		
		for ( y = y0; y <= y1; y++ ) {
			lcdWriteData( color );
		}
	}
}

/******************************************************************************
* Function Name  : LCD_DrawLine
* Description    : Bresenham's line algorithm
* Input          : - x1: A�������
*                  - y1: A������� 
*				   - x2: B�������
*				   - y2: B������� 
*				   - color: ����ɫ
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/	 
void lcdDrawLine( uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1 , uint16_t color )
{
	int dx =  abs(x1-x0), sx = x0<x1 ? 1 : -1;
	int dy = -abs(y1-y0), sy = y0<y1 ? 1 : -1; 
	int err = dx+dy, e2; /* error value e_xy */
	
	for(;;){  /* loop */
	lcdSetPixel(x0,y0, color);
	if (x0==x1 && y0==y1) break;
	e2 = 2*err;
	if (e2 >= dy) { err += dy; x0 += sx; } /* e_xy+e_x > 0 */
	if (e2 <= dx) { err += dx; y0 += sy; } /* e_xy+e_y < 0 */
	}
} 

/******************************************************************************
* Function Name  : PutChar
* Description    : ��Lcd��������λ����ʾһ���ַ�
* Input          : - Xpos: ˮƽ��� 
*                  - Ypos: ��ֱ���  
*				   - ASCI: ��ʾ���ַ�
*				   - charColor: �ַ���ɫ   
*				   - bkColor: ������ɫ 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdDrawChar( uint16_t Xpos, uint16_t Ypos, uint8_t ASCII, uint16_t charColor)
{
	uint16_t i, j;
    uint8_t buffer[16], tmp_char;
    GetASCIICode(buffer,ASCII);  /* ȡ��ģ��� */
    for( i=0; i<16; i++ )
    {
        tmp_char = buffer[i];
        for( j=0; j<8; j++ )
        {
            if( (tmp_char >> 7 - j) & 0x01 == 0x01 )
            {
                lcdSetPixel( Xpos + j, Ypos + i, charColor );  /* �ַ���ɫ */
            }
        }
    }
}

/******************************************************************************
* Function Name  : GUI_Text
* Description    : ��ָ�������ʾ�ַ�
* Input          : - Xpos: �����
*                  - Ypos: ����� 
*				   - str: �ַ�
*				   - charColor: �ַ���ɫ   
*				   - bkColor: ������ɫ 
* Output         : None
* Return         : None
* Attention		 : None
*******************************************************************************/
void lcdDrawText(uint16_t Xpos, uint16_t Ypos, uint8_t *str,uint16_t Color)
{
    uint8_t TempChar;
    do
    {
        TempChar = *str++;  
        lcdDrawChar( Xpos, Ypos, TempChar, Color );
        if( Xpos < LCD_MAX_X - 8 )
        {
            Xpos += 8;
        } 
        else if ( Ypos < LCD_MAX_Y - 16 )
        {
            Xpos = 0;
            Ypos += 16;
        }   
        else
        {
            Xpos = 0;
            Ypos = 0;
        }    
    }
    while ( *str != 0 );
}  

/*********************************************************************************************************
      END FILE
*********************************************************************************************************/
