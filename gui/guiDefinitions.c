#include "guiDefinitions.h"
#include "../lcd/lcd.h"
#include "../lcd/lcdExtension.h"


GUIPanel panelList[20];
uint8_t panelCounter = 0;


uint16_t getTextWidth ( char *str ) {
	uint16_t width = 0;

	while ( *str != 0 ) {
		width++;
		str++;
	}

	return width;
}


uint8_t guiAddPanel ( GUIRectangle coords, GUIColorDecoration colors, GUIHorizontalAlignment horizontalAlignment, GUIVerticalAlignment verticalAlignment, GUIPanelOrientation orientation, GUIButtonVerticalAlignment buttonVerticalAlignment, GUIButtonHorizontalAlignment buttonHorizontalAlignment, uint8_t padding, uint8_t elementSpacing, uint8_t visible ) {
	if ( panelCounter > 19 ) {
		return GUI_INFINITY;
	}
	panelList[panelCounter].coords = coords;
	panelList[panelCounter].colors = colors;
	panelList[panelCounter].horizontalAlignment = horizontalAlignment;
	panelList[panelCounter].verticalAlignment = verticalAlignment;
	panelList[panelCounter].orientation = orientation;
	panelList[panelCounter].buttonVerticalAlignment = buttonVerticalAlignment;
	panelList[panelCounter].buttonHorizontalAlignment = buttonHorizontalAlignment;
	panelList[panelCounter].padding = padding;
	panelList[panelCounter].elementSpacing = elementSpacing;
	panelList[panelCounter].visible = visible;
	panelList[panelCounter].widgetCount = 0;
	panelList[panelCounter].buttonCount = 0;
	panelList[panelCounter].textCount = 0;

	return panelCounter++;
}


uint8_t guiAddButton( uint8_t panel, GUIRectangle coords, GUIColorDecoration idleColors, GUIColorDecoration activeColors, GUIButtonState state, uint8_t padding, uint8_t *text, uint8_t visible, void (*onPressHandler)( uint16_t x, uint16_t y, GUIButtonWidget *cButton ), void (*onLeaveHandler)( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) ) {
	if ( panelList[panel].widgetCount > 13 || panelList[panel].buttonCount > 10 ) {
		return GUI_INFINITY;
	}
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.coords = coords;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.idleColors = idleColors;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.activeColors = activeColors;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.padding = padding;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.text = text;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].base.visible = visible;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].state = state;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].parentPanel = panel;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].buttonIndex = panelList[panel].buttonCount;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].onPressHandler = onPressHandler;
	panelList[panel].buttonWidgetsList[panelList[panel].buttonCount].onLeaveHandler = onLeaveHandler;
	panelList[panel].widgetsList[panelList[panel].widgetCount].index = panelList[panel].buttonCount;
	panelList[panel].widgetsList[panelList[panel].widgetCount].type = BUTTON;
	panelList[panel].widgetCount++;

	return panelList[panel].buttonCount++;
}


uint8_t guiAddText( uint8_t panel, GUIRectangle coords, GUIColorDecoration colors, GUITextAlignment align, uint8_t padding, uint8_t *text, uint8_t visible ) {
	if ( panelList[panel].widgetCount > 13 || panelList[panel].textCount > 4 ) {
		return GUI_INFINITY;
	}
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.coords = coords;
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.idleColors = colors;
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.idleColors.bgColor = panelList[panel].colors.bgColor;
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.padding = padding;
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.text = text;
	panelList[panel].textWidgetsList[panelList[panel].textCount].base.visible = visible;
	panelList[panel].textWidgetsList[panelList[panel].textCount].align = align;
	panelList[panel].widgetsList[panelList[panel].widgetCount].index = panelList[panel].textCount;
	panelList[panel].widgetsList[panelList[panel].widgetCount].type = TEXT;
	panelList[panel].widgetCount++;

	return panelList[panel].textCount++;
}


void checkWidget ( GUIWidget *widget ) {
	uint16_t textWidth = (getTextWidth( widget->text ))*8;
	if ( textWidth + (widget->padding+1)*2 > widget->coords.width ) {
		widget->coords.width = textWidth + (widget->padding+1)*2;
	}
	if ( 16 + (widget->padding+1)*2 > widget->coords.height ) {
		widget->coords.height = 16 + (widget->padding+1)*2;
	}
}

void guiDrawButton ( GUIButtonWidget *button ) {
	if ( button->base.visible == 0 ) {
		return;
	}
	
	// Check the text width
	checkWidget( &(button->base) );
	
	// xt, yt - left top corner of the text
	uint16_t xt, yt;
	uint32_t textWidth = (getTextWidth( button->base.text ))*8;

	// If too small, it should have been stretched to exact text width.
	xt = button->base.coords.x0 + button->base.padding + 1 + ((button->base.coords.width - 2*(button->base.padding + 1) - textWidth)/2);

	// Same as above.
	yt = button->base.coords.y0 + button->base.padding + 1 + ((button->base.coords.height - 2*(button->base.padding + 1) - 16)/2);

	// ColorDecoration based switch.
	switch ( button->state ) {
		case IDLE:
			lcdDrawFilledRectangle( button->base.coords.x0, button->base.coords.y0, button->base.coords.x0 + button->base.coords.width - 1, button->base.coords.y0 + button->base.coords.height - 1, button->base.idleColors.borderColor, button->base.idleColors.bgColor );
			lcdDrawText( xt, yt, button->base.text, button->base.idleColors.textColor );
		break;
		case ACTIVE:
			lcdDrawFilledRectangle( button->base.coords.x0, button->base.coords.y0, button->base.coords.x0 + button->base.coords.width - 1, button->base.coords.y0 + button->base.coords.height - 1, button->base.activeColors.borderColor, button->base.activeColors.bgColor );
			lcdDrawText( xt, yt, button->base.text, button->base.activeColors.textColor );
	}
}


void guiDrawText ( GUITextWidget *text ) {
	if ( text->base.visible == 0 ) {
		return;
	}
	uint16_t xt, yt;
	uint16_t textWidth = (getTextWidth( text->base.text ))*8;
	checkWidget( &(text->base) );

	xt = text->base.coords.x0 + text->base.padding + 1 + ((text->base.coords.width - 2*(text->base.padding + 1) - textWidth)/2);

	yt = text->base.coords.y0 + text->base.padding + 1 + ((text->base.coords.height - 2*(text->base.padding + 1) - 16)/2);

	lcdFillRectangularArea( text->base.coords.x0, text->base.coords.y0, text->base.coords.x0 + text->base.coords.width - 1, text->base.coords.y0 + text->base.coords.height - 1, text->base.idleColors.bgColor );
	lcdDrawText( xt, yt, text->base.text, text->base.idleColors.textColor );
}

void guiCalculatePanel ( GUIPanel *cPanel ) {
	if ( cPanel->visible == 0 ) {
		return;
	}

	GUIButtonWidget *cButton;
	GUITextWidget *cText;
	
	
	uint8_t i;
	uint16_t maxWidth = 0, maxHeight = 0, totalWidth = 0, totalHeight = 0;
	
	// Container variables, variables to contain current computed coordinates, used to sum all that needed.
	// Default values for both possibilities - vertical and horizontal alignment.
	uint16_t currentX, currentY, startWidth, startHeight;

	/*
	 * Initial if computing maximum values needed to draw a panel.
	 */
	if ( cPanel->orientation == PANEL_VERTICAL ) {
		/*
		 * If alignment is vertical, after checking widget we need compute maximum width of the widget.
		 */
		
		for ( i = 0; i < cPanel->widgetCount; i++ ) {
			if ( cPanel->widgetsList[i].type == TEXT ) {
				checkWidget( &(cPanel->textWidgetsList[cPanel->widgetsList[i].index].base) );
				totalHeight += cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.height + cPanel->elementSpacing;
				
				if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.width > maxWidth && cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					maxWidth = cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.width;
				}
			} else {
				checkWidget( &(cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base) );
				totalHeight += cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.height + cPanel->elementSpacing;
				
				if ( cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.width > maxWidth && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					maxWidth = cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.width;
				}
			}
		}
		
		startWidth = maxWidth + cPanel->padding*2 + 2;
		startHeight = cPanel->padding*2 + 2;

		// Computed height and width summarized give width and height of the whole panel.
		
		if ( cPanel->coords.width < startWidth ) {
			cPanel->coords.width = startWidth;
		}
		
		if ( cPanel->coords.height < startHeight + totalHeight - cPanel->elementSpacing ) {
			cPanel->coords.height = startHeight + totalHeight - cPanel->elementSpacing; //One additional element spacing removed from the bottom.
		}
	} else {
		/*
		 * Else we compute maxHeight.
		 */
		
		for ( i = 0; i < cPanel->widgetCount; i++ ) {
			if ( cPanel->widgetsList[i].type == TEXT ) {
				checkWidget( &(cPanel->textWidgetsList[cPanel->widgetsList[i].index].base) );
				totalWidth += cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.width + cPanel->elementSpacing;
				
				if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.height > maxHeight && cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					maxHeight = cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.coords.height;
				}
			} else {
				checkWidget( &(cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base) );
				totalWidth += cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.width + cPanel->elementSpacing;
				
				if ( cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.height > maxHeight && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					maxHeight = cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.coords.height;
				}
			}
		}
		
		startWidth = cPanel->padding*2 + 2;
		startHeight = maxHeight + cPanel->padding*2 + 2;

		if ( cPanel->coords.width < startWidth + totalWidth - cPanel->elementSpacing ) {
			cPanel->coords.width = startWidth + totalWidth - cPanel->elementSpacing; //One additional element spacing removed from the bottom.
		}
		
		if ( cPanel->coords.height < startHeight ) {
			cPanel->coords.height = startHeight;
		}
	}
	
	
	if ( cPanel->horizontalAlignment == ALIGN_HORIZONTAL_LEFT ) {
		cPanel->coords.x0 = 0;
	} else if ( cPanel->horizontalAlignment == ALIGN_HORIZONTAL_RIGHT ) {
		cPanel->coords.x0 = LCD_MAX_X - cPanel->coords.width;
	} else if ( cPanel->horizontalAlignment == ALIGN_HORIZONTAL_CENTER ) {
		cPanel->coords.x0 = (LCD_MAX_X - cPanel->coords.width)/2;
	}
	if ( cPanel->verticalAlignment == ALIGN_VERTICAL_TOP ) {
		cPanel->coords.y0 = 0;
	} else if ( cPanel->verticalAlignment == ALIGN_VERTICAL_BOTTOM ) {
		cPanel->coords.y0 = LCD_MAX_Y - cPanel->coords.height;
	} else if ( cPanel->verticalAlignment == ALIGN_VERTICAL_CENTER ) {
		cPanel->coords.y0 = (LCD_MAX_Y - cPanel->coords.height)/2;
	}
	

	if ( cPanel->orientation == PANEL_VERTICAL ) {
		switch ( cPanel->buttonVerticalAlignment ) {
			case BUTTONALIGN_VERTICAL_TOP:
				currentX = cPanel->coords.x0 + cPanel->padding + 1;
				currentY = cPanel->coords.y0 + cPanel->padding + 1;
			break;

			case BUTTONALIGN_VERTICAL_BOTTOM:
				currentX = cPanel->coords.x0 + cPanel->padding + 1;
				currentY = cPanel->coords.y0 + cPanel->padding + 1 + (cPanel->coords.height - cPanel->padding*2 - 2 - (totalHeight - cPanel->elementSpacing));
			break;

			case BUTTONALIGN_VERTICAL_CENTER:
				currentX = cPanel->coords.x0 + cPanel->padding + 1;
				currentY = cPanel->coords.y0 + cPanel->padding + 1 + (cPanel->coords.height - cPanel->padding*2 - 2 - (totalHeight - cPanel->elementSpacing))/2;
			break;
		}
	} else {
		switch ( cPanel->buttonHorizontalAlignment ) {
			case BUTTONALIGN_HORIZONTAL_LEFT:
				currentX = cPanel->coords.x0 + cPanel->padding + 1;
				currentY = cPanel->coords.y0 + cPanel->padding + 1;
			break;

			case BUTTONALIGN_HORIZONTAL_RIGHT:
				currentX = cPanel->coords.x0 + cPanel->padding + 1 + (cPanel->coords.width - cPanel->padding*2 - 2 - (totalWidth - cPanel->elementSpacing));
				currentY = cPanel->coords.y0 + cPanel->padding + 1;
			break;

			case BUTTONALIGN_HORIZONTAL_CENTER:
				currentX = cPanel->coords.x0 + cPanel->padding + 1 + (cPanel->coords.width - cPanel->padding*2 - 2 - (totalWidth - cPanel->elementSpacing))/2;
				currentY = cPanel->coords.y0 + cPanel->padding + 1;
		}
	}


	if ( cPanel->orientation == PANEL_VERTICAL ) {
		if ( cPanel->buttonHorizontalAlignment == BUTTONALIGN_HORIZONTAL_LEFT ) {

			// Loop iterating widgets, computing new coordinates.
			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX;
					cButton->base.coords.y0 = currentY;

					currentY += cButton->base.coords.height + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					if ( cText->align == TEXTALIGN_CENTER ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width)/2;
						cText->base.coords.y0 = currentY;
					} else if ( cText->align == TEXTALIGN_RIGHT ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width);
						cText->base.coords.y0 = currentY;
					} else {
						cText->base.coords.x0 = currentX;
						cText->base.coords.y0 = currentY;
					}

					currentY += cText->base.coords.height + cPanel->elementSpacing;
				}
			}
		} else if ( cPanel->buttonHorizontalAlignment == BUTTONALIGN_HORIZONTAL_RIGHT ) {
			// Align to the right side. The rest is analogous to the case above.

			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cButton->base.coords.width);
					cButton->base.coords.y0 = currentY;

					currentY += cButton->base.coords.height + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					if ( cText->align == TEXTALIGN_CENTER ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width)/2;
						cText->base.coords.y0 = currentY;
					} else if ( cText->align == TEXTALIGN_RIGHT ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width);
						cText->base.coords.y0 = currentY;
					} else {
						cText->base.coords.x0 = currentX;
						cText->base.coords.y0 = currentY;
					}

					currentY += cText->base.coords.height + cPanel->elementSpacing;
				}
			}
		} else if ( cPanel->buttonHorizontalAlignment == BUTTONALIGN_HORIZONTAL_CENTER ) {
			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cButton->base.coords.width)/2;
					cButton->base.coords.y0 = currentY;

					currentY += cButton->base.coords.height + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					if ( cText->align == TEXTALIGN_CENTER ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width)/2;
						cText->base.coords.y0 = currentY;
					} else if ( cText->align == TEXTALIGN_RIGHT ) {
						cText->base.coords.x0 = currentX + (cPanel->coords.width - cPanel->padding*2 - 2 - cText->base.coords.width);
						cText->base.coords.y0 = currentY;
					} else {
						cText->base.coords.x0 = currentX;
						cText->base.coords.y0 = currentY;
					}

					currentY += cText->base.coords.height + cPanel->elementSpacing;
				}
			}
		}
	} else {
		if ( cPanel->buttonVerticalAlignment == BUTTONALIGN_VERTICAL_CENTER ) {
			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX;
					cButton->base.coords.y0 = currentY + (cPanel->coords.height - cPanel->padding*2 - 2 - cButton->base.coords.height)/2;

					currentX += cButton->base.coords.width + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					cText->base.coords.x0 = currentX;
					cText->base.coords.y0 = currentY + (cPanel->coords.height - cPanel->padding*2 - 2 - cText->base.coords.height)/2;

					currentX += cText->base.coords.width + cPanel->elementSpacing;
				}
			}
		} else if ( cPanel->buttonVerticalAlignment == BUTTONALIGN_VERTICAL_TOP ) {
			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX;
					cButton->base.coords.y0 = currentY;

					currentX += cButton->base.coords.width + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					cText->base.coords.x0 = currentX;
					cText->base.coords.y0 = currentY + (cPanel->coords.height - cPanel->padding*2 - 2 - cText->base.coords.height)/2;

					currentX += cText->base.coords.width + cPanel->elementSpacing;
				}
			}
		} else if ( cPanel->buttonVerticalAlignment == BUTTONALIGN_VERTICAL_BOTTOM ) {
			for ( i = 0; i < cPanel->widgetCount; i++ ) {
				if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];

					cButton->base.coords.x0 = currentX;
					cButton->base.coords.y0 = currentY + (cPanel->coords.height - cPanel->padding*2 - 2 - cButton->base.coords.height);

					currentX += cButton->base.coords.width + cPanel->elementSpacing;
				} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
					cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];

					cText->base.coords.x0 = currentX;
					cText->base.coords.y0 = currentY + (cPanel->coords.height - cPanel->padding*2 - 2 - cText->base.coords.height)/2;

					currentX += cText->base.coords.width + cPanel->elementSpacing;
				}
			}
		}
	}
}


void guiDrawPanel ( uint8_t panel ) {
	// Pointers to current objects to work on.
	GUIPanel *cPanel;
	GUIButtonWidget *cButton;
	GUITextWidget *cText;

	uint8_t i;

	//Checking if the panel that is requested exists...
	if ( panel < panelCounter ) {
		cPanel = &(panelList[panel]);
	} else {
		return;
	}

	if ( cPanel->visible == 0 ) {
		return;
	}
	
	guiCalculatePanel( cPanel );

	lcdDrawFilledRectangle( cPanel->coords.x0, cPanel->coords.y0, cPanel->coords.x0 + cPanel->coords.width - 1, cPanel->coords.y0 + cPanel->coords.height - 1, cPanel->colors.borderColor, cPanel->colors.bgColor );
	
	for ( i = 0; i < cPanel->widgetCount; i++ ) {
		if ( cPanel->widgetsList[i].type == BUTTON && cPanel->buttonWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
			cButton = &cPanel->buttonWidgetsList[cPanel->widgetsList[i].index];
			
			guiDrawButton( cButton );
		} else if ( cPanel->textWidgetsList[cPanel->widgetsList[i].index].base.visible != 0 ) {
			cText = &cPanel->textWidgetsList[cPanel->widgetsList[i].index];
			cText->base.idleColors.bgColor = cPanel->colors.bgColor;
			
			guiDrawText( cText );
		}
	}
}

void guiRedraw ( uint8_t level ) {
	if ( level >= panelCounter ) {
		return;
	}
	
	for ( ; level < panelCounter; level++ ) {
		guiDrawPanel( level );
	}
}

void guiRedrawPanelThatMayCollide ( uint8_t panel ) {
	if ( panel >= panelCounter ) {
		return;
	}
	
	GUIPanel *cPanel = &(panelList[panel]), *cPanelTemp;

	if ( cPanel->visible == 0 ) {
		return;
	}

	guiDrawPanel( panel );
	panel++;
	
	for ( ; panel < panelCounter; panel++ ) {
		cPanelTemp = &(panelList[panel]);
		
		if (
			cPanelTemp->visible != 0
			&&
			cPanel->coords.x0 < cPanelTemp->coords.x0 + cPanelTemp->coords.width
			&&
			cPanel->coords.x0 + cPanel->coords.width > cPanelTemp->coords.x0
			&&
			cPanel->coords.y0 < cPanelTemp->coords.y0 + cPanelTemp->coords.height
			&&
			cPanel->coords.y0 + cPanel->coords.height > cPanelTemp->coords.y0
		) {
			guiRedrawPanelThatMayCollide( panel );
		}
	}
}

void guiRedrawButtonThatMayCollide ( uint8_t panel, uint8_t button ) {
	if ( panel >= panelCounter || button >= panelList[panel].buttonCount ) {
		return;
	}
	
	GUIPanel *cPanel = &(panelList[panel]), *cPanelTemp;
	GUIButtonWidget *cButton = &(cPanel->buttonWidgetsList[button]);

	if ( cButton->base.visible == 0 ) {
		return;
	}

	guiDrawButton( cButton );
	panel++;
	
	for ( ; panel < panelCounter; panel++ ) {
		cPanelTemp = &(panelList[panel]);
		
		if (
			cPanelTemp->visible != 0
			&&
			cButton->base.coords.x0 < cPanelTemp->coords.x0 + cPanelTemp->coords.width
			&&
			cButton->base.coords.x0 + cButton->base.coords.width > cPanelTemp->coords.x0
			&&
			cButton->base.coords.y0 < cPanelTemp->coords.y0 + cPanelTemp->coords.height
			&&
			cButton->base.coords.y0 + cButton->base.coords.height > cPanelTemp->coords.y0
		) {
			guiRedrawPanelThatMayCollide( panel );
		}
	}
}

uint8_t guiGetPanelCounter () {
	return panelCounter;
}

GUIPanel* guiGetPanel ( uint8_t panel ) {
	if ( panel >= panelCounter || panel >= 20 ) {
		return 0;
	}
	
	return &(panelList[panel]);
}

GUIButtonWidget* guiGetButton ( uint8_t panel, uint8_t bIndex ) {
	if ( panel >= panelCounter ) {
		return 0;
	}
	if ( bIndex >= panelList[panel].buttonCount || bIndex >= 10 ) {
		return 0;
	}
	
	return &(panelList[panel].buttonWidgetsList[bIndex]);
}

GUITextWidget* guiGetText( uint8_t panel, uint8_t tIndex ) {
	if ( panel >= panelCounter ) {
		return 0;
	}
	if ( tIndex >= panelList[panel].textCount || tIndex >= 4 ) {
		return 0;
	}

	return &(panelList[panel].textWidgetsList[tIndex]);
}

uint8_t isBetween( uint16_t what, uint16_t x0, uint16_t x1 ) {
	if ( what >= x0 && what <= x1 ) {
		return 1;
	}
	return 0;
}
