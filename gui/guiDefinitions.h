/**
 * \file guiDefinitions.h
 * \brief File contains complete GUI API for Graphical LCD using lcd.h-alike API.
 *
 * It consists of all needed data structures and functions for drawing and calculating the GUI on the
 * LCD.
 */

#include "../lcd/lcd.h"
#include "../lcd/lcdExtension.h"

/**
 * Define for errors in functions returning numbers.
 */
#define GUI_INFINITY 200


#pragma once

/**
 * Contains the type of the widget.
 */
enum GUIWidgetType {
	TEXT,
	BUTTON
};

typedef enum GUIWidgetType GUIWidgetType;

/**
 * Indicates in what state a GUIButtonWidget is.
 */
enum GUIButtonState {
	IDLE,
	ACTIVE
};

typedef enum GUIButtonState GUIButtonState;


/**
 * Indicates orientation of the panel and the way the buttons will be placed.
 */
enum GUIPanelOrientation {
	PANEL_HORIZONTAL,
	PANEL_VERTICAL
};

typedef enum GUIPanelOrientation GUIPanelOrientation;

/**
 * Indicates what alignment GUITextWidget should have in a panel.
 */
enum GUITextAlignment {
	TEXTALIGN_LEFT,
	TEXTALIGN_RIGHT,
	TEXTALIGN_CENTER
};

typedef enum GUITextAlignment GUITextAlignment;

/**
 * Indicates what horizontal alignment GUIButtonWidgets should have in the panel.
 */
enum GUIButtonHorizontalAlignment {
	BUTTONALIGN_HORIZONTAL_LEFT,
	BUTTONALIGN_HORIZONTAL_RIGHT,
	BUTTONALIGN_HORIZONTAL_CENTER
};

typedef enum GUIButtonHorizontalAlignment GUIButtonHorizontalAlignment;

/**
 * Indicates what vertical alignment GUIButtonWidget should have in the panel.
 */
enum GUIButtonVerticalAlignment {
	BUTTONALIGN_VERTICAL_TOP,
	BUTTONALIGN_VERTICAL_BOTTOM,
	BUTTONALIGN_VERTICAL_CENTER
};

typedef enum GUIButtonVerticalAlignment GUIButtonVerticalAlignment;

/**
 * Indicates what horizontal alignment (relatively to the screen) a panel should have.
 */
enum GUIHorizontalAlignment {
	ALIGN_HORIZONTAL_LEFT,
	ALIGN_HORIZONTAL_RIGHT,
	ALIGN_HORIZONTAL_CENTER,
	ALIGN_HORIZONTAL_CUSTOM
};

typedef enum GUIHorizontalAlignment GUIHorizontalAlignment;

/**
 * Indicates what vertical alignment (relatively to the screen) a panel should have.
 */
enum GUIVerticalAlignment {
	ALIGN_VERTICAL_TOP,
	ALIGN_VERTICAL_BOTTOM,
	ALIGN_VERTICAL_CENTER,
	ALIGN_VERTICAL_CUSTOM
};

typedef enum GUIVerticalAlignment GUIVerticalAlignment;

/**
 * Rectangle coordinates structure used to contain coordinates for every GUI control.
 */
struct GUIRectangle {
	uint16_t x0;
	uint16_t y0;
	uint16_t width;
	uint16_t height;
};

typedef struct GUIRectangle GUIRectangle;

/**
 * A structure containing color, it merges three types of color used in almost every drawing action.
 */
struct GUIColorDecoration {
	uint16_t borderColor;
	uint16_t bgColor;
	uint16_t textColor;
};

typedef struct GUIColorDecoration GUIColorDecoration;

/**
 * Base structure for GUITextWidget and GUIButtonWidget, contains very basic properties that are common
 * for both type of widgets.
 *
 * \note Not documented fields are either self-explanatory or described in data type documentation.
 */
struct GUIWidget {
	/**
	 * Coordinates of the widget. Programmer can only define width and height, location will be overwritten
	 * when panels will be calculated. If width or height is too small to defined padding and text width/height,
	 * the button will be stretched to proper size.
	 */
	GUIRectangle coords;
	GUIColorDecoration idleColors;
	GUIColorDecoration activeColors;
	/**
	 * Inner margin.
	 */
	uint8_t padding;
	/**
	 * Text to be displayed on the widget.
	 */
	uint8_t *text;
	/**
	 * Visibility flag: 0 => invisible, any other value => visible.
	 */
	uint8_t visible;
};

typedef struct GUIWidget GUIWidget;

/**
 * Basic active GUI control.
 *
 * \note Not documented fields are either self-explanatory or described in data type documentation.
 */
struct GUIButtonWidget {
	GUIWidget base;
	GUIButtonState state;
	/**
	 * Index of parent panel in the panel list.
	 */
	uint8_t parentPanel;
	/**
	 * Index of the button itself on its panel's GUIButtonWidget list.
	 */
	uint8_t buttonIndex;
	/**
	 * Handler function executed every press event.
	 */
	void (*onPressHandler)( uint16_t x, uint16_t y, struct GUIButtonWidget *cButton );
	/**
	 * Handler function executed every leave event.
	 */
	void (*onLeaveHandler)( uint16_t x, uint16_t y, struct GUIButtonWidget *cButton );
};

typedef struct GUIButtonWidget GUIButtonWidget;

/**
 * Basic inactive GUI control, created for information purposes. Besides being inactive (it does not
 * have handlers!) it differs from GUIButtonWidget in having no border drawn. It also uses only idleColors
 * from the base.
 *
 * \note Not documented fields are either self-explanatory or described in data type documentation.
 */
struct GUITextWidget {
	GUIWidget base;
	GUITextAlignment align;
};

typedef struct GUITextWidget GUITextWidget;

/**
 * Simple structure for use in a general widget list in every panel.
 */
struct GUIWidgetsOrder {
	uint8_t index;
	GUIWidgetType type;
};

typedef struct GUIWidgetsOrder GUIWidgetsOrder;

/**
 * \struct GUIPanel
 * Basic top structure of the GUI.
 *
 * \note Not documented fields are either self-explanatory or described in data type documentation.
 */
typedef struct GUIPanel {
	/**
	 * Coordinates, if GUIHorizontalAlignment is not set to CUSTOM x is ignored and overwritten by calculated value.
	 * If GUIVerticalAlignment is not set to CUSTOM same will happen to y. If width is not enough to contain the widest element
	 * width is ignored and calculated for this maximum value, same will happen with height if it is not big enough.
	 */
	GUIRectangle coords;
	GUIColorDecoration colors;
	GUIHorizontalAlignment horizontalAlignment;
	GUIVerticalAlignment verticalAlignment;
	GUIPanelOrientation orientation;
	GUIButtonVerticalAlignment buttonVerticalAlignment;
	GUIButtonHorizontalAlignment buttonHorizontalAlignment;
	/**
	 * Array of GUITextWidget elements, may contain maximum 4 per panel.
	 */
	GUITextWidget textWidgetsList[4];
	/**
	 * Array of GUIButtonWidget elements, may contain maximum 10 per panel.
	 */
	GUIButtonWidget buttonWidgetsList[10];
	/**
	 * Array of widgets order in the panel, necessary for displaying properly the whole panel.
	 * 14 widgets per panel is possible.
	 */
	GUIWidgetsOrder widgetsList[14];
	/**
	 * Visibility flag: 0 => invisible, any other value => visible.
	 */
	uint8_t visible;
	uint8_t widgetCount;
	uint8_t buttonCount;
	uint8_t textCount;
	/**
	 * Inner margin.
	 */
	uint8_t padding;
	/**
	 * Distance between consecutive widgets.
	 */
	uint8_t elementSpacing;
} GUIPanel;

/**
 * Adds a panel to the list of panels. There may be up to 20 panels in the application.
 *
 * \return Returns the index of the currently added panel in the panel list or
 * #GUI_INFINITY if trying to add 21st panel.
 *
 * \param coords Coordinates of the panel, details: #GUIPanel#coords, #GUIRectangle.
 * \param colors Colors for the panel, details: #GUIColorDecoration.
 * \param horizontalAlignment Details: #GUIPanel#coords, #GUIHorizontalAlignment.
 * \param verticalAlignment Details: #GUIPanel#coords, #GUIVerticalAlignment.
 * \param orientation Details: #GUIPanelOrientation.
 * \param buttonVerticalAlignment Details: #GUIButtonVerticalAlignment.
 * \param buttonHorizontalAlignment Details: #GUIButtonHorizontalAlignment.
 * \param padding Inner margin.
 * \param elementSpacing Distance between consecutive elements' borders.
 * \param visible Indicates if panel starts being visible or not.
 */
uint8_t guiAddPanel( GUIRectangle coords, GUIColorDecoration colors, GUIHorizontalAlignment horizontalAlignment, GUIVerticalAlignment verticalAlignment, GUIPanelOrientation orientation, GUIButtonVerticalAlignment buttonVerticalAlignment, GUIButtonHorizontalAlignment buttonHorizontalAlignment, uint8_t padding, uint8_t elementSpacing, uint8_t visible );

/**
 * Adds a button to specific panel.
 *
 * \return Returns the index of currently added button in the button list or
 * #GUI_INFINITY if trying to add 11th button.
 *
 * \param panel Panel index in the panel list.
 * \param coords Coordinates of the button, user can define only width and height,
 * the rest will be overwritten for drawing, details: #GUIRectangle, #GUIWidget#coords.
 * \param idleColors Colors used in the #GUIButtonState IDLE state.
 * \param activeColors Colors used in the #GUIButtonState ACTIVE state.
 * \param state Default state assigned when button is being created.
 * \param padding Inner margin.
 * \param text C-string that is supposed to appear on the button.
 * \param visible Default visibility assigned when button is being created.
 * \param onPressHandler onPress event handler.
 * \param onLeaveHandler onLeave event handler.
 */
uint8_t guiAddButton( uint8_t panel, GUIRectangle coords, GUIColorDecoration idleColors, GUIColorDecoration activeColors, GUIButtonState state, uint8_t padding, uint8_t *text, uint8_t visible, void (*onPressHandler)( uint16_t x, uint16_t y, GUIButtonWidget *cButton ), void (*onLeaveHandler)( uint16_t x, uint16_t y, GUIButtonWidget *cButton ) );

/**
 * Adds a text to specific panel.
 *
 * \return Returns the index of currently added text in the text list or
 * #GUI_INFINITY if trying to ad 5th text.
 *
 * \param panel Panel index in the panel list.
 * \param coords Coordinates of the text, user can define only width and height,
 * the rest will be overwritten for drawing, details: #GUIRectangle, #GUIWidget#coords.
 * \param colors Colors used to draw the widget.
 * \param align Text widget alignment in the displayed widgets set.
 * \param padding Inner margin.
 * \param text C-string that is supposed to appear on the text widget.
 * \param visible Default visibility assigned when text is being created.
 */
uint8_t guiAddText( uint8_t panel, GUIRectangle coords, GUIColorDecoration colors, GUITextAlignment align, uint8_t padding, uint8_t *text, uint8_t visible );

/**
 * Returns a pointer to the panel of given index. May be used to modify existing
 * GUI controls. If trying to get non existing panel or a panel that is
 * out of range - a null pointer is returned.
 *
 * \return Pointer to the desired panel specified by #panel.
 *
 * \param panel Index of wanted panel in the panel list.
 */
GUIPanel* guiGetPanel( uint8_t panel );

/**
 * Similar to guiGetPanel() except it returns a pointer to specific button.
 *
 * \return Pointer to the desired button. If trying to get non existing button or a button that is
 * out of range - a null pointer is returned.
 *
 * \param panel Index of the panel in which the desired button exists.
 * \param bIndex Index of the desired button in the button list in its parent panel.
 */
GUIButtonWidget* guiGetButton( uint8_t panel, uint8_t bIndex );

/**
 * Similar to guiGetPanel() except it returns a pointer to specific text.
 *
 * \return Pointer to the desired text. If trying to get non existing text or a text that is
 * out of range - a null pointer is returned.
 *
 * \param panel Index of the panel in which the desired text exists.
 * \param bIndex Index of the desired button in the text list in its parent panel.
 */
GUITextWidget* guiGetText( uint8_t panel, uint8_t tIndex );

/**
 * Draws a button indicated by the pointer.
 *
 * \param button Pointer to the button that is needed to be drawn.
 */
void guiDrawButton( GUIButtonWidget *button );

/**
 * Draws a text indicated by the pointer.
 *
 * \param button Pointer to the text that is needed to be drawn.
 */
void guiDrawText( GUITextWidget *text );

/**
 * Draws a panel indicated by the pointer. It also calculates coordinates of all widgets existing
 * in the panel and overwrites their coordinates and that should never(!) be changed!
 *
 * \param button Pointer to the button that is needed to be drawn.
 */
void guiDrawPanel( uint8_t panel );

/**
 * Calculates coordinates for widgets in specified panel.
 *
 * \param cPanel Pointer to the panel that needs calculations.
 */
void guiCalculatePanel ( GUIPanel *cPanel );

/**
 * Redraws all GUI controls beginning from the bottom level given as a parameter.
 *
 * \param level Level on which function should start redrawing, 0 means redrawing whole GUI.
 */
void guiRedraw( uint8_t level );

/**
 * Redraws a panel of given index checking if it collides with any other. It redraws given panel
 * and all above it that collide.
 *
 * \param panel Index of the panel that needs redrawing in the panel list.
 */
void guiRedrawPanelThatMayCollide( uint8_t panel );

/**
 * Similar to guiRedrawPanelThatMayCollide() except it draws a button given by two indexes and
 * all panels that collide with it.
 *
 * \param panel Index of the parent panel to the button that needs redrawing.
 * \param button Index of the button in its parent panel button list.
 */
void guiRedrawButtonThatMayCollide( uint8_t panel, uint8_t button );

/**
 * Returns number of panels already created in the GUI.
 *
 * \return Number of panels in the application.
 */
uint8_t guiGetPanelCounter();

/**
 * Auxiliary function that is used to check whether argument what is between values
 * x0 and x1. Inequalities are weak.
 *
 * \return 1 if what parameter is between x0 and x1, 0 if what parameter isn't between x0 and x1.
 *
 * \param what Checked value.
 * \param x0 Start of the interval.
 * \param x1 End of the interval.
 */
uint8_t isBetween( uint16_t what, uint16_t x0, uint16_t x1 );
