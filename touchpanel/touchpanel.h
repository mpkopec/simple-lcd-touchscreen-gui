/**
 * \file touchpanel.h
 * \brief File contains all functions needed to read data from the touchscreen controller.
 * \author AVRman (http://www.powermcu.com)
 * \author Maciej Kope� (corrections, refactoring, new functions)
 * \version 1.1b
 *
 */

#ifndef _TOUCHPANEL_H_
#define _TOUCHPANEL_H_

#include "LPC17xx.h"

/**
 * \struct Coordinate
 * This structure contains coordinates of a point using the smallest type that may be used.
 */
typedef	struct Coordinate
{
   uint16_t x;
   uint16_t y;
} Coordinate;

/**
 * \struct Matrix
 * This structure contains multiplicative constants calculated at calibration necessary for
 * the data read from the touchscreen. It is responsible for transforming 12-bit number to coordinates
 * that suit users LCD configuration such as rotation.
 */
typedef struct Matrix 
{						
long double An,  
            Bn,     
            Cn,   
            Dn,    
            En,    
            Fn,     
            Divider;
} Matrix;

/**
 * Variable containing calibration #Matrix, should be used to calculate every "real" coordinates.
 */
extern Matrix matrix;


//-------------------------------------------------------------------------
/**
 * \cond
 * Irrelevant for programmer.
 */
extern Coordinate ScreenSample[3];
extern Coordinate DisplaySample[3];
extern Coordinate  display;


#define	CHX 	        0x90
#define	CHY 	        0xd0

#define SSPSR_RNE       2
#define SSPSR_BSY       4


#define SPI_SPEED_4MHz    18 	  /* 4MHz */
#define SPI_SPEED_2MHz    36 	  /* 2MHz */
#define	SPI_SPEED_1MHz	  72	  /* 1MHz */
#define	SPI_SPEED_500kHz  144	  /* 500kHz */
#define SPI_SPEED_400kHz  180	  /* 400kHz */


#define TP_CS(a)	if (a)	\
					LPC_GPIO0->FIOSET = (1<<6);\
					else		\
					LPC_GPIO0->FIOCLR = (1<<6)

#define TP_INT_IN   ( LPC_GPIO2->FIOPIN & (1<<13) ) 
/**
 * \endcond
 */
//-------------------------------------------------------------------------


/**
 * Indicates number of samples for touchpanelReadTapCountingMean().
 */
#define TOUCHPANEL_MEAN_SAMPLES 50

/**
 * This define contains acceptable error of read data given in pixels. Necessary for
 * touchpanelReadTapAvoidingError().
 */
#define TOUCHPANEL_ERROR 2

/**
 * Function that initializes touchscreen. To use the touchscreen you must use this
 * function before doing any operation on the touchscreen controller.
 */
void touchpanelInit( void );

/**
 * Function reads digital values of x and y coordinates from the touchscreen. It takes 9 samples
 * then counts 3 means from three samples and calculates differences between them. If this differences
 * are greater than #TOUCHPANEL_ERROR then 0 is returned.
 *
 * \return Function returns 0 if touchscreen isn't touched or if error of the read data is greater
 * than #TOUCHPANEL_ERROR and 1 if the readout was successful.
 *
 * \param rawCoordinatePtr Pointer to the variable of output coordinates.
 */
uint8_t touchpanelReadTapAvoidingError( Coordinate *rawCoordinatePtr );

/**
 * Function very similar to touchpanelReadTapAvoidingError() except it simply counts mean value from
 * 50 samples and assumes that this is the right value.
 *
 * \warning Should not be used in calibration function! It may cause big errors in #Matrix coefficients.
 * This may lead to highly incorrect readout from the touchpanelGetTapPoint() and then to make event handling
 * impossible.
 * \return Function returns 1 if the touchscreen is pressed and the readout was successful, and it returns 0
 * if touchscreen isn't pressed.
 *
 * \param rawCoordinatePtr Pointer to the variable of output coordinates.
 */
uint8_t touchpanelReadTapCountingMean( Coordinate *rawCoordinatePtr );

/**
 * Function responsible for filling up the calibration matrix from 3 points. It uses
 * touchpanelSetCalibrationMatrix() to calculate needed values.
 */
void touchpanelCalibrate(void);

/**
 * Auxiliary function for drawing crosses with the center position of given arguments. It is used
 * to point the place for user to tap in the calibration process.
 *
 * \param Xpos X coordinate of the cross center.
 * \param Ypos Y coordinate of the cross center.
 */
void touchpanelDrawCross(uint16_t Xpos,uint16_t Ypos);

/**
 * \cond
 */

/**
 * Basing on the coordinates of a point and got result from the touchscreen, it calculates coefficients needed in
 * touchpanelGetTapPoint().
 *
 * \param displayPtr Indicates what "real" coordinates tapped point has.
 * \param screenPtr Coordinates got from the touchscreen for the #displayPtr point.
 * \param matrixPtr Pointer to the matrix itself, need to be static or global in the file range, should be saved
 * for the time of program execution.
 */
uint8_t touchpanelSetCalibrationMatrix( Coordinate * displayPtr,Coordinate * screenPtr );

/**
 * \endcond
 */

/**
 * Function gets "real" coordinates basing on calibration matrix and given results from the touchscreen.
 *
 * \warning Should not be used to check if user tapped the screen, it doesn't work that way! It only
 * transforms one coordinate set to another, should be used after programmer is sure that screen was pressed
 * and has raw data of the tap, see touchpanelReadTapCountingMean() and touchpanelReadTapAvoidingError().
 *
 * \return Returns 0 if succeeded and 1 if #Matrix Divider = 0.
 *
 * \param displayPtr Pointer to the output variable containing coordinates after conversion from raw data
 * from the touchpanel.
 * \param screenPtr Input pointer of raw data got from the touchscrenn with touchpanelReadTapCountingMean()
 * or touchpanelReadTapAvoidingError().
 */
uint8_t touchpanelGetTapPoint( Coordinate * displayPtr, Coordinate * screenPtr );

#endif

