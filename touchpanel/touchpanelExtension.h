/**
 * \file touchpanelExtension.h
 * \brief File contains the event propagation mechanism.
 *
 * It is responsible for finding tapped widgets and pass the event to their handlers. Two types
 * of events may occur in this version:
 * - onPress - occurs when user presses the touchscreen, it is handled all the time if the user keeps
 * his stylus pressing the screen,
 * - onLeave - occurs once when user removes his finger from the screen.
 *
 * If pressing is not over any part of the GUI - control is passed to default handlers of both type
 * of event.
 */

#include "touchpanel/touchpanel.h"
#include "lcd/lcd.h"
#include "lcd/lcdExtension.h"
#include "gui/guiDefinitions.h"

#pragma once

/**
 * \enum ObjectType
 * Enum contains objects indicating type of object. It is used to contain information about on what type of
 * object previous event occurred. It is necessary because of the order of searching certain objects in
 * GUI lists. Names are self-explanatory.
 */
typedef enum ObjectType {
	BUTTON_OBJECT,
	/// To be implemented
	CANVAS_OBJECT,
	DEFAULT_OBJECT,
	EMPTY_OBJECT
} ObjectType;

/**
 * Function responsible for event propagation itself. It iterates GUI elements and when it finds widget,
 * which coordinates match the tap coordinates it passes execution to specific handler defined as a pointer
 * in the button widget. If no matching widget is found and the event didn't occur on any panel, proper
 * default handler is executed.
 *
 * Additionally, function checks if the state of the button that was under event changed, if so it redraws it
 * safely.
 *
 * @param defaultPressHandler A pointer to the default handler executed every pressing (also continuous)
 * on the screen, that is not over any of the GUI controls.
 * @param defaulLeaveHandler A pointer to the default handler executed every leaving the touchscreen, if
 * the touch wasn't over any of the GUI controls.
 */
void checkTap ( void (*defaultPressHandler)( uint16_t x, uint16_t y ), void (*defaultLeaveHandler)( uint16_t x, uint16_t y ) );

