#include "touchpanel/touchpanel.h"
#include "touchpanel/touchpanelExtension.h"
#include "lcd/lcd.h"
#include "lcd/lcdExtension.h"
#include "gui/guiDefinitions.h"


void checkTap ( void (*defaultPressHandler)( uint16_t x, uint16_t y ), void (*defaultLeaveHandler)( uint16_t x, uint16_t y ) ) {
	static GUIButtonWidget *pButton = 0;
	static ObjectType pObject = EMPTY_OBJECT;
	static Coordinate pCoords = {.x = 0xffff, .y=0xffff};
	
	Coordinate *displayPtr, display;
	displayPtr = &display;
	uint8_t check = touchpanelReadTapCountingMean( displayPtr );
	
	if ( check != 0 ) {
		touchpanelGetTapPoint( displayPtr, displayPtr );
		
		uint8_t panelCounter;
		uint8_t bCounter;
		uint8_t found = 0;
		
		for ( panelCounter = guiGetPanelCounter(); panelCounter > 0; panelCounter-- ) {
			GUIPanel *cPanel = guiGetPanel( panelCounter - 1 );
			
			if (
				isBetween( displayPtr->x, cPanel->coords.x0, cPanel->coords.x0 + cPanel->coords.width )
				&&
				isBetween( displayPtr->y, cPanel->coords.y0, cPanel->coords.y0 + cPanel->coords.height )
				&&
				cPanel->visible != 0
			) {
				if ( found == 0 ) { // If an earlier tap occurred on a panel function should not propagate further
					for ( bCounter = cPanel->buttonCount; bCounter > 0; bCounter-- ) {
						GUIButtonWidget *cButton = &(cPanel->buttonWidgetsList[bCounter - 1]);
						
						if (
						isBetween( displayPtr->x, cButton->base.coords.x0, cButton->base.coords.x0 + cButton->base.coords.width )
						&&
						isBetween( displayPtr->y, cButton->base.coords.y0, cButton->base.coords.y0 + cButton->base.coords.height )
						&&
						cButton->base.visible != 0
						) {
							GUIButtonState pState = cButton->state;
							pButton = cButton;
							pObject = BUTTON_OBJECT;
							pCoords.x = displayPtr->x;
							pCoords.y = displayPtr->y;

							if ( cButton->onPressHandler != (void *)0 ) {
								cButton->onPressHandler( displayPtr->x, displayPtr->y, cButton );
							}

							if ( cButton->state != pState ) {
								guiRedrawButtonThatMayCollide( cButton->parentPanel, cButton->buttonIndex );
							}

							break;
						}
					}
				}
				found = 1;
			}
		}
		
		if ( !found ) {
			if ( defaultPressHandler != 0 ) {
				defaultPressHandler( displayPtr->x, displayPtr->y );
			}
			
			pObject = DEFAULT_OBJECT;
			pCoords.x = displayPtr->x;
			pCoords.y = displayPtr->y;
		}
	} else {
		if ( pObject == BUTTON_OBJECT && pButton != 0 && pButton->onLeaveHandler != 0 ) {
			GUIButtonState pState = pButton->state;
			
			pButton->onLeaveHandler( pCoords.x, pCoords.y, pButton );
			
			if ( pButton->state != pState ) {
				guiRedrawButtonThatMayCollide( pButton->parentPanel, pButton->buttonIndex );
			}
			
			pObject = EMPTY_OBJECT;
			return;
		} else if ( pObject == DEFAULT_OBJECT && defaultLeaveHandler != 0 ) {
			defaultLeaveHandler( pCoords.x, pCoords.y );
			
			pObject = EMPTY_OBJECT;
			return;
		}
	}
}
