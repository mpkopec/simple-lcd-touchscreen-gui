/****************************************Copyright (c)****************************************************
 **
 **                                 http://www.powermcu.com
 **
 **--------------File Info---------------------------------------------------------------------------------
 ** File name:               TouchPanel.c
 ** Descriptions:            The TouchPanel application function
 **
 **--------------------------------------------------------------------------------------------------------
 ** Created by:              AVRman
 ** Created date:            2010-11-7
 ** Version:                 v1.0
 ** Descriptions:            The original version
 **
 **--------------------------------------------------------------------------------------------------------
 ** Modified by:			Maciej Kopec
 ** Modified date:			2013-10-15
 ** Version:				1.1b
 ** Descriptions:
 **
 *********************************************************************************************************/

/* Includes ------------------------------------------------------------------*/
#include "../lcd/lcd.h"
#include "touchpanel.h"

/* Private variables ---------------------------------------------------------*/
Matrix matrix;
Coordinate display;

/* DisplaySample LCD����϶�Ӧ��ads7843����ADֵ �磺LCD ���45,45 Ӧ�õ�X Y����ADC�ֱ�Ϊ3388,920 */
Coordinate ScreenSample[3];
/* LCD�ϵ���� */
Coordinate DisplaySample[3] = { { 110, 60 }, { 20, 220 }, { 220, 220 } };

/* Private define ------------------------------------------------------------*/

/*******************************************************************************
 * Function Name  : LPC17xx_SPI_SetSpeed
 * Description    : Set clock speed to desired value
 * Input          : - speed: speed
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
void LPC17xx_SPI_SetSpeed(uint8_t speed) {
	speed &= 0xFE;
	if (speed < 2) {
		speed = 2;
	}
	LPC_SSP1->CPSR = speed;
}

/*******************************************************************************
 * Function Name  : ADS7843_SPI_Init
 * Description    : ADS7843 SPI ��ʼ��
 * Input          : None
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
static void touchpanelDriverDataBusInit(void) {
	volatile uint32_t dummy;

	/* Initialize and enable the SSP1 Interface module. */LPC_SC->PCONP |= (1
			<< 10); /* Enable power to SSPI1 block  */

	/* P0.7 SCK, P0.8 MISO, P0.9 MOSI are SSP pins. */LPC_PINCON->PINSEL0
			&= ~((3UL << 14) | (3UL << 16) | (3UL << 18)); /* P0.7,P0.8,P0.9 cleared */
	LPC_PINCON->PINSEL0 |= (2UL << 14) | (2UL << 16) | (2UL << 18); /* P0.7 SCK1,P0.8 MISO1,P0.9 MOSI1 */

	/* PCLK_SSP1=CCLK */LPC_SC->PCLKSEL0 &= ~(3 << 20); /* PCLKSP0 = CCLK/4 (18MHz) */
	LPC_SC->PCLKSEL0 |= (1 << 20); /* PCLKSP0 = CCLK   (72MHz) */

	LPC_SSP1->CR0 = 0x0007; /* 8Bit, CPOL=0, CPHA=0         */
	LPC_SSP1->CR1 = 0x0002; /* SSP1 enable, master          */

	LPC17xx_SPI_SetSpeed(SPI_SPEED_500kHz);

	/* wait for busy gone */
	while (LPC_SSP1->SR & (1 << SSPSR_BSY))
		;

	/* drain SPI RX FIFO */
	while (LPC_SSP1->SR & (1 << SSPSR_RNE)) {
		dummy = LPC_SSP1->DR;
	}
}

/*******************************************************************************
 * Function Name  : TP_Init
 * Description    : ADS7843�˿ڳ�ʼ��
 * Input          : None
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
void touchpanelInit(void) {
	LPC_GPIO0->FIODIR |= (1 << 6); /* P0.6 CS is output */
	LPC_GPIO2->FIODIR |= (0 << 13); /* P2.13 TP_INT is input */
	TP_CS(1);
	touchpanelDriverDataBusInit();
}

/*******************************************************************************
 * Function Name  : DelayUS
 * Description    : ��ʱ1us
 * Input          : - cnt: ��ʱֵ
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
static void touchpanelDelayUS(uint32_t cnt) {
	uint32_t i;
	for (i = 0; i < cnt; i++) {
		uint8_t us = 12; /* ����ֵΪ12����Լ��1΢�� */
		while (us--) /* ��1΢��	*/
		{
			;
		}
	}
}

/*******************************************************************************
 * Function Name  : WR_CMD
 * Description    : �� ADS7843д���
 * Input          : - cmd: ��������
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
static uint8_t touchpanelWriteCmd(uint8_t cmd) {
	uint8_t byte_r;

	while (LPC_SSP1->SR & (1 << SSPSR_BSY))
		; /* Wait for transfer to finish */
	LPC_SSP1->DR = cmd;
	while (LPC_SSP1->SR & (1 << SSPSR_BSY))
		; /* Wait for transfer to finish */
	while (!(LPC_SSP1->SR & (1 << SSPSR_RNE)))
		; /* Wait untill the Rx FIFO is not empty */
	byte_r = LPC_SSP1->DR;

	return byte_r; /* Return received value */
}

/*******************************************************************************
 * Function Name  : RD_AD
 * Description    : ��ȡADCֵ
 * Input          : None
 * Output         : None
 * Return         : ADS7843���ض��ֽ����
 * Attention		 : None
 *******************************************************************************/
static int touchpanelReadAddress(void) {
	unsigned short buf, temp;

	temp = touchpanelWriteCmd(0x00);
	buf = temp << 8;
	touchpanelDelayUS(1);
	temp = touchpanelWriteCmd(0x00);
	;
	buf |= temp;
	buf >>= 3;
	buf &= 0xfff;
	return buf;
}

/*******************************************************************************
 * Function Name  : Read_X
 * Description    : ��ȡADS7843ͨ��X+��ADCֵ
 * Input          : None
 * Output         : None
 * Return         : ADS7843����ͨ��X+��ADCֵ
 * Attention		 : None
 *******************************************************************************/
int touchpanelReadX(void) {
	int i;
	TP_CS(0)
		;
	touchpanelDelayUS(1);
	touchpanelWriteCmd(CHX);
	touchpanelDelayUS(1);
	i = touchpanelReadAddress();
	TP_CS(1)
		;
	return i;
}

/*******************************************************************************
 * Function Name  : Read_Y
 * Description    : ��ȡADS7843ͨ��Y+��ADCֵ
 * Input          : None
 * Output         : None
 * Return         : ADS7843����ͨ��Y+��ADCֵ
 * Attention		 : None
 *******************************************************************************/
int touchpanelReadY(void) {
	int i;
	TP_CS(0)
		;
	touchpanelDelayUS(1);
	touchpanelWriteCmd(CHY);
	touchpanelDelayUS(1);
	i = touchpanelReadAddress();
	TP_CS(1)
		;
	return i;
}

/*******************************************************************************
 * Function Name  : TP_GetAdXY
 * Description    : ��ȡADS7843 ͨ��X+ ͨ��Y+��ADCֵ
 * Input          : None
 * Output         : None
 * Return         : ADS7843���� ͨ��X+ ͨ��Y+��ADCֵ
 * Attention		 : None
 *******************************************************************************/
void touchpanelGetXY(int *x, int *y) {
	int adx, ady;
	adx = touchpanelReadX();
	touchpanelDelayUS(1);
	ady = touchpanelReadY();
	*x = adx;
	*y = ady;
}

/*******************************************************************************
 * Function Name  : DrawCross
 * Description    : ��ָ����껭ʮ��׼��
 * Input          : - Xpos: Row Coordinate
 *                  - Ypos: Line Coordinate
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
void touchpanelDrawCross(uint16_t Xpos, uint16_t Ypos) {
	lcdDrawLine(Xpos - 15, Ypos, Xpos - 2, Ypos, 0xffff);
	lcdDrawLine(Xpos + 2, Ypos, Xpos + 15, Ypos, 0xffff);
	lcdDrawLine(Xpos, Ypos - 15, Xpos, Ypos - 2, 0xffff);
	lcdDrawLine(Xpos, Ypos + 2, Xpos, Ypos + 15, 0xffff);

	lcdDrawLine(Xpos - 15, Ypos + 15, Xpos - 7, Ypos + 15,
			RGB565CONVERT(184,158,131));
	lcdDrawLine(Xpos - 15, Ypos + 7, Xpos - 15, Ypos + 15,
			RGB565CONVERT(184,158,131));

	lcdDrawLine(Xpos - 15, Ypos - 15, Xpos - 7, Ypos - 15,
			RGB565CONVERT(184,158,131));
	lcdDrawLine(Xpos - 15, Ypos - 7, Xpos - 15, Ypos - 15,
			RGB565CONVERT(184,158,131));

	lcdDrawLine(Xpos + 7, Ypos + 15, Xpos + 15, Ypos + 15,
			RGB565CONVERT(184,158,131));
	lcdDrawLine(Xpos + 15, Ypos + 7, Xpos + 15, Ypos + 15,
			RGB565CONVERT(184,158,131));

	lcdDrawLine(Xpos + 7, Ypos - 15, Xpos + 15, Ypos - 15,
			RGB565CONVERT(184,158,131));
	lcdDrawLine(Xpos + 15, Ypos - 15, Xpos + 15, Ypos - 7,
			RGB565CONVERT(184,158,131));
}

/*******************************************************************************
 * Function Name  : Read_Ads7846
 * Description    : �õ��˲�֮���X Y
 * Input          : None
 * Output         : None
 * Return         : Coordinate�ṹ���ַ
 * Attention		 : None
 *******************************************************************************/
uint8_t touchpanelReadTapAvoidingError( Coordinate *rawCoordinatePtr ) {
	int m0, m1, m2, TP_X[1], TP_Y[1], temp[3];
	uint8_t count = 0;
	int buffer[2][9] = { { 0 }, { 0 } }; /* ���X��Y���ж�β��� */
	do /* ѭ������9�� */
	{
		touchpanelGetXY(TP_X, TP_Y);
		buffer[0][count] = TP_X[0];
		buffer[1][count] = TP_Y[0];
		count++;
	} while (!TP_INT_IN && count < 9); /* TP_INT_INΪ�������ж����,���û����������ʱTP_INT_IN�ᱻ�õ� */
	if (count == 9) /* �ɹ�����9��,�����˲� */
	{
		/* Ϊ����������,�ֱ��3��ȡƽ��ֵ */
		temp[0] = (buffer[0][0] + buffer[0][1] + buffer[0][2]) / 3;
		temp[1] = (buffer[0][3] + buffer[0][4] + buffer[0][5]) / 3;
		temp[2] = (buffer[0][6] + buffer[0][7] + buffer[0][8]) / 3;
		/* ����3����ݵĲ�ֵ */
		m0 = temp[0] - temp[1];
		m1 = temp[1] - temp[2];
		m2 = temp[2] - temp[0];
		/* ��������ֵȡ���ֵ */
		m0 = m0 > 0 ? m0 : (-m0);
		m1 = m1 > 0 ? m1 : (-m1);
		m2 = m2 > 0 ? m2 : (-m2);
		/* �жϾ�Բ�ֵ�Ƿ񶼳����ֵ���ޣ������3����Բ�ֵ����������ֵ�����ж���β����ΪҰ��,�������㣬��ֵ����ȡΪ2 */
		if (m0 > TOUCHPANEL_ERROR && m1 > TOUCHPANEL_ERROR && m2
				> TOUCHPANEL_ERROR) {
			return 0;
		}
		/* �������ǵ�ƽ��ֵ��ͬʱ��ֵ��screen */
		if (m0 < m1) {
			if (m2 < m0)
				rawCoordinatePtr->x = (temp[0] + temp[2]) / 2;
			else
				rawCoordinatePtr->x = (temp[0] + temp[1]) / 2;
		} else if (m2 < m1)
			rawCoordinatePtr->x = (temp[0] + temp[2]) / 2;
		else
			rawCoordinatePtr->x = (temp[1] + temp[2]) / 2;

		/* ͬ�� ����Y��ƽ��ֵ */
		temp[0] = (buffer[1][0] + buffer[1][1] + buffer[1][2]) / 3;
		temp[1] = (buffer[1][3] + buffer[1][4] + buffer[1][5]) / 3;
		temp[2] = (buffer[1][6] + buffer[1][7] + buffer[1][8]) / 3;
		m0 = temp[0] - temp[1];
		m1 = temp[1] - temp[2];
		m2 = temp[2] - temp[0];
		m0 = m0 > 0 ? m0 : (-m0);
		m1 = m1 > 0 ? m1 : (-m1);
		m2 = m2 > 0 ? m2 : (-m2);
		if (m0 > TOUCHPANEL_ERROR && m1 > TOUCHPANEL_ERROR && m2
				> TOUCHPANEL_ERROR) {
			return 0;
		}

		if (m0 < m1) {
			if (m2 < m0)
				rawCoordinatePtr->y = (temp[0] + temp[2]) / 2;
			else
				rawCoordinatePtr->y = (temp[0] + temp[1]) / 2;
		} else if (m2 < m1)
			rawCoordinatePtr->y = (temp[0] + temp[2]) / 2;
		else
			rawCoordinatePtr->y = (temp[1] + temp[2]) / 2;
		return 1;
	}
	return 0;
}

uint8_t touchpanelReadTapCountingMean( Coordinate *rawCoordinatePtr ) {
	int sumx, sumy, TP_X[1], TP_Y[1];
	uint8_t i, count = 0;
	int buffer[2][TOUCHPANEL_MEAN_SAMPLES] = { { 0 }, { 0 } };
	do {
		touchpanelGetXY(TP_X, TP_Y);
		buffer[0][count] = TP_X[0];
		buffer[1][count] = TP_Y[0];
		count++;
	} while (!TP_INT_IN && count < TOUCHPANEL_MEAN_SAMPLES);
	if (count == TOUCHPANEL_MEAN_SAMPLES) {
		sumx = 0;
		sumy = 0;
		for (i = 0; i < count; i++) {
			sumx += buffer[0][i];
			sumy += buffer[1][i];
		}
		rawCoordinatePtr->x = sumx / count;
		rawCoordinatePtr->y = sumy / count;

		return 1;
	}
	return 0;
}

/*******************************************************************************
 * Function Name  : setCalibrationMatrix
 * Description    : ����� K A B C D E F
 * Input          : None
 * Output         : None
 * Return         : ����1��ʾ�ɹ� 0ʧ��
 * Attention		 : None
 *******************************************************************************/
uint8_t touchpanelSetCalibrationMatrix(Coordinate * displayPtr,
		Coordinate * screenPtr) {
	Matrix *matrixPtr = &matrix;

	uint8_t retTHRESHOLD = 0;
	/* K��(X0��X2) (Y1��Y2)��(X1��X2) (Y0��Y2) */
	matrixPtr->Divider = ((screenPtr[0].x - screenPtr[2].x) * (screenPtr[1].y
			- screenPtr[2].y)) - ((screenPtr[1].x - screenPtr[2].x)
			* (screenPtr[0].y - screenPtr[2].y));
	if (matrixPtr->Divider == 0) {
		retTHRESHOLD = 1;
	} else {
		/* A��((XD0��XD2) (Y1��Y2)��(XD1��XD2) (Y0��Y2))��K	*/
		matrixPtr->An = ((displayPtr[0].x - displayPtr[2].x) * (screenPtr[1].y
				- screenPtr[2].y)) - ((displayPtr[1].x - displayPtr[2].x)
				* (screenPtr[0].y - screenPtr[2].y));
		/* B��((X0��X2) (XD1��XD2)��(XD0��XD2) (X1��X2))��K	*/
		matrixPtr->Bn = ((screenPtr[0].x - screenPtr[2].x) * (displayPtr[1].x
				- displayPtr[2].x)) - ((displayPtr[0].x - displayPtr[2].x)
				* (screenPtr[1].x - screenPtr[2].x));
		/* C��(Y0(X2XD1��X1XD2)+Y1(X0XD2��X2XD0)+Y2(X1XD0��X0XD1))��K */
		matrixPtr->Cn = (screenPtr[2].x * displayPtr[1].x - screenPtr[1].x
				* displayPtr[2].x) * screenPtr[0].y + (screenPtr[0].x
				* displayPtr[2].x - screenPtr[2].x * displayPtr[0].x)
				* screenPtr[1].y + (screenPtr[1].x * displayPtr[0].x
				- screenPtr[0].x * displayPtr[1].x) * screenPtr[2].y;
		/* D��((YD0��YD2) (Y1��Y2)��(YD1��YD2) (Y0��Y2))��K	*/
		matrixPtr->Dn = ((displayPtr[0].y - displayPtr[2].y) * (screenPtr[1].y
				- screenPtr[2].y)) - ((displayPtr[1].y - displayPtr[2].y)
				* (screenPtr[0].y - screenPtr[2].y));
		/* E��((X0��X2) (YD1��YD2)��(YD0��YD2) (X1��X2))��K	*/
		matrixPtr->En = ((screenPtr[0].x - screenPtr[2].x) * (displayPtr[1].y
				- displayPtr[2].y)) - ((displayPtr[0].y - displayPtr[2].y)
				* (screenPtr[1].x - screenPtr[2].x));
		/* F��(Y0(X2YD1��X1YD2)+Y1(X0YD2��X2YD0)+Y2(X1YD0��X0YD1))��K */
		matrixPtr->Fn = (screenPtr[2].x * displayPtr[1].y - screenPtr[1].x
				* displayPtr[2].y) * screenPtr[0].y + (screenPtr[0].x
				* displayPtr[2].y - screenPtr[2].x * displayPtr[0].y)
				* screenPtr[1].y + (screenPtr[1].x * displayPtr[0].y
				- screenPtr[0].x * displayPtr[1].y) * screenPtr[2].y;
	}
	return (retTHRESHOLD);
}

/*******************************************************************************
 * Function Name  : getDisplayPoint
 * Description    : ͨ�� K A B C D E F ��ͨ��X Y��ֵת��ΪҺ�������
 * Input          : None
 * Output         : None
 * Return         : ����1��ʾ�ɹ� 0ʧ��
 * Attention		 : None
 *******************************************************************************/
uint8_t touchpanelGetTapPoint(Coordinate * displayPtr, Coordinate * screenPtr) {
	uint8_t retTHRESHOLD = 0;

	if (matrix.Divider != 0) {
		/* XD = AX+BY+C */
		displayPtr->x = ((matrix.An * screenPtr->x) + (matrix.Bn
				* screenPtr->y) + matrix.Cn) / matrix.Divider;
		/* YD = DX+EY+F */
		displayPtr->y = ((matrix.Dn * screenPtr->x) + (matrix.En
				* screenPtr->y) + matrix.Fn) / matrix.Divider;
	} else {
		retTHRESHOLD = 1;
	}
	return (retTHRESHOLD);
}

/*******************************************************************************
 * Function Name  : TouchPanel_Calibrate
 * Description    : У׼������
 * Input          : None
 * Output         : None
 * Return         : None
 * Attention		 : None
 *******************************************************************************/
void touchpanelCalibrate(void) {
	uint8_t i, check = 0;
	Coordinate point;

	for (i = 0; i < 3; i++) {
		lcdClear(LCDBlack);
		lcdDrawText(10, 10, "Touch crosshair to calibrate", 0xffff);
		touchpanelDelayUS(1000 * 500);
		touchpanelDrawCross(DisplaySample[i].x, DisplaySample[i].y);
		do {
			check = touchpanelReadTapAvoidingError( &point );
		} while (check ==  0);
		ScreenSample[i].x = point.x;
		ScreenSample[i].y = point.y;
	}
	touchpanelSetCalibrationMatrix(&DisplaySample[0], &ScreenSample[0]); /* ����ֵ�õ����� */
	lcdClear(LCDBlack);
}

/*********************************************************************************************************
 END FILE
 *********************************************************************************************************/
